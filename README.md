# MyCandidature

A place for Organizations to Manage Hiring of Candidates and have a test generated on-the-go as soon as candidate details are entered.


## Features

### How it Works

- There are 3 types of users: 
    - Superadmin
        - Add, Modify & Delete: 
            - Questions
            - Organizations 
            - Skills
    - Organization admin
        - Add Candidate details & Generate test
        - View Result & Answers of Candidate
    - Candidate
        - Gets a link for the test through an email
        - Can give the test after uploading their profile picture
        
- Superadmin, when creates an organization, an email having login credentials is sent to the org admin
- Org admin, when creates a test & adds candidate details, an email containing test link is sent to the Candidate.
- Org admin gets emails when a candidate starts & ends their test.
- Upon completion of the test candidate also gets a `Thank You` email.


## Technical Details

### Tech Stack

- Django (Web Framework)
- Postgresql (Database)
- SQLAlchemy (ORM for the project)
    - All the SQL queries are written with SQLAlchemy in mind
    - Has great performance compared to Django's default ORM
- Alembic
    - DB migration tool, to use with SQLAlchemy
    - To generate migrations from models & reflect those migrations in the database
- Jinja
    - Web Template Engine
    - Better performance in terms of rendering than Django's default Template Engine
- RabbitMQ
    - Message Broker
    - To send and receive tasks/messages
- Celery
    - Asynchronous task queue, to run async tasks
    - Used here to send emails in the background


### Workflow

#### Directory Structure

- `src/` 
    - Root of the application

- `src/apps/main/`
    - Contains all the logic of components in separate directories related to
        - Migrations
        - Static files (CSS, JS, & Plugins (Bootstrap4, SB-Admin2))   
        - Models' logic
        - Views
        - Celery Tasks (Sending emails)
        - HTML Templates (layouts & pages for screens)
        - Alembic & Jinja configurations
    - `urls.py`
        - Since we're using Class-Based views, the URL pattern will be `/className/functionName`
    - `middleware/admin_auth_middleware.py`
        - A middeware, that intercepts incoming requests & responses
    -  `migratoins/versions/`
        - Contains all the Alembic migrations
        - File Naming Convention: `revisionID_description.py`
    - `models/`
        - `base.py` contains SQLAlchemy driver URL & session
        - Rest of the files have inherited `base.py`'s `BaseModel` class and are having model structure & definitions to access data using SQLAlchemy
    - `tasks/send_email.py` contains a Celery task that would send an email
    -  `views/base_view.py` has decorators' definitions & the `BaseView` class handles requests from routes        

- `src/config/config.yml`
    - Contains (Django) App's SECRET Key, Email & Database configurations  
    - `settings.py` and other files will reference to it and fetch credentials to be used in the app.

- `src/my_candidature/`
    - `celery.py` Contains celery (to run async tasks like sending emails in background) configurations.
    -  `settings.py` has all the global settings of the app
    - `urls.py` refers to `apps/main/`'s URLs & specifies root directory to store MEDIA files.


## Installation Instructions

- After cloning the repository, execute following commands
```
cd my-candidature-web

# Create & Activate virtual environment
virtualenv env
source env/bin/activate

# Install all the packages
pip install -r requirements.txt
```

### Configuring the Database

- Run following commands to install postgresql in the system:
```
sudo apt-get update
sudo apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib
```

#### Create the database

```
# Login to postgres session
sudo -u postgres psql

# Create a database named mycandidature 
CREATE DATABASE mycandidature;

# myprojectuser = username & 'password' = password
CREATE USER myprojectuser WITH PASSWORD 'password';
ALTER ROLE myprojectuser SET client_encoding TO 'utf8';
ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed';
ALTER ROLE myprojectuser SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE myproject TO myprojectuser;

# Press \q to exit the postgres session
\q
```
- Go inside `src/apps/main` directory and run migrations using `alembic upgrade head`


### For running asynchronous tasks (Sending Emails in background)
- Install RabbitMQ server:
    `apt-get install rabbitmq-server`
   - To start the RabbitMQ Server:
        `systemctl start rabbitmq-server`
   - To check the status of RabbitMQ Server:
        `systemctl status rabbitmq-server`
- Run `celery -A my_candidature worker --loglevel=DEBUG --concurrency=1 -l info` to run celery, once rabbitmq-server is active.


### Set Credentials & Run the App

- Make a copy of `src/config/config.sample.yml` file and name it `config.yml`
     - Edit `config.yml` by updating it with your secret key, and other database credentials.

- Go inside `src/` directory and run `python manage.py runserver`

- Once the server is live, login to this URL: `/user/login`
     - A default Superadmin user has already been created
          - Superadmin credentials:
               - Email: `super@mycandidature.com`
               - Password: `admin`




## Development Instructions

### Alembic - Generating Migrations

- Once models are changed for any component, run following commands to create its migrations file & reflect it into the database
```
cd src/apps/main

alembic revision --autogenerate -m "Write description of migration here"
```
<!--alembic revision --autogenerate -m "Write description of migration here"-->

- Rename file generated inside `main/migrations/versions/` with the following naming convention: `revisionID_description`
    - ex: `0005_add_contact_number_in_org_table.py`
    - Also update the Revision ID `revision` & `down_revision` in the generated file to match the version number 

- Run `alembic upgrade head` to have this new migration reflected in the database.

- Note: Once you delete all the tables (i.e, when freshly migrating to DB), run `alembic upgrade head` to have all the changes reflected in the database.
