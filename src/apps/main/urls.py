"""my_candidature URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from apps.main.views.organization_view import OrganizationView
from apps.main.views.question_view import QuestionView
from apps.main.views.result_view import ResultView
from apps.main.views.skill_view import SkillView
from apps.main.views.test_view import TestView
from apps.main.views.user_view import UserView

app_name = 'apps'

urlpatterns = [
    url(r'^user($|/)', UserView.as_view()),
    url(r'^question($|/)', QuestionView.as_view()),
    url(r'^organization($|/)', OrganizationView.as_view()),
    url(r'^skill($|/)', SkillView.as_view()),
    url(r'^test($|/)', TestView.as_view()),
    url(r'^result($|/)', ResultView.as_view()),
]
