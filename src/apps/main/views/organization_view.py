import math
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpRequest, JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.text import slugify
from django.template.response import TemplateResponse
from sqlalchemy.exc import IntegrityError

from apps.main.models.organization import Organization
from apps.main.models.user import User
from apps.main.models.user_role import UserRole, RoleType
from apps.main.models.test import Test
from apps.main.models.result import Result
from apps.main.tasks.send_email import send_email
from apps.main.views.base_view import BaseView, is_super_admin
from my_candidature.settings import FROM_EMAIL, CONFIG


class OrganizationView(BaseView):
    @is_super_admin
    def add(self, request):
        if request.method == 'GET':
            return render(request, 'pages/organization/add.html')
        elif request.method == 'POST':
            request_dict = {x: request.POST.get(x) for x in request.POST.keys()}
            created_by = request.session['user_id']
            organization_dict = {}
            organization_dict['legal_name'] = request_dict['legal_name']
            organization_dict['slug'] = slugify(request_dict['legal_name'])
            organization_dict['created_by'] = created_by
            organization_dict['display_name'] = request_dict['display_name']
            organization_dict['contact_email'] = request_dict['contact_email']
            organization_dict['contact_phone'] = request_dict['contact_phone']
            organization_dict['contact_address'] = request_dict['contact_address']
            organization_dict['city'] = request_dict['city']
            try:
                email = request_dict['contact_email']
                user = User.get_by_email(email)
                if user is not None:
                    messages.add_message(request, messages.ERROR, "An account with this email already exist.")
                    return render(request, 'pages/organization/add.html', request_dict)

                organization = Organization.add(organization_dict)
                url = CONFIG['BASE_URL'] + '/user/login'
                user_dict = {
                    'first_name': request_dict['first_name'],
                    'last_name': request_dict['last_name'],
                    'email': email,
                    'mobile_number': request_dict['contact_phone'],
                    'gender': None,
                    'created_by': created_by,
                }

                user, password = User.add(user_dict)  # xiTHXn
                # print(password)
                user_role_dict = {'role_id': RoleType.ORG_ADMIN,
                                  'user_id': user.id,
                                  'org_id': organization.id}
                UserRole.add(user_role_dict)
                msg_string = request_dict['legal_name'] + " added successfully"
            except AssertionError as error:
                messages.add_message(request, messages.ERROR, error)
                return render(request, 'pages/organization/add.html', request_dict)
            except Exception as error:
                if isinstance(error, IntegrityError):
                    messages.add_message(request, messages.ERROR, "Organization already exists.")
                else:
                    messages.add_message(request, messages.ERROR, "Something went wrong")
                return render(request, 'pages/organization/add.html', request_dict)
            try:
                html_message = render_to_string('pages/email/email-add-organisation.html',
                                                {'organization': organization,
                                                 'password': password,
                                                 'url': url})
                subject = "You have been on-boarded to My Candidature"
                recipient_list = [organization.contact_email]

                send_email.delay(subject = subject, message = '', from_email = FROM_EMAIL,
                                 recipient_list = recipient_list,
                                 fail_silently = False, html_message = html_message)
            except:
                msg_string = request_dict['legal_name'] + " added successfully, could not send email."
            messages.success(request, msg_string)
            return HttpResponseRedirect('/organization/list')

    @is_super_admin
    def list(self, request):
        page = {'current_page': None,
                'next': None,
                'prev': None,
                'last_page': None,
                'stages': 2,
                'is_paginated': False
                }
        records_per_page = 10
        try:
            page_number = request.GET.get('page')
            page_number = int(page_number)
            if page_number < 1:
                page_number = 1
            page_number -= 1
        except Exception:
            page_number = 0

        total = Organization.get_total()
        total_pages = math.ceil((total / records_per_page))
        if page_number >= total_pages:
            page_number = 0
        organization_list = Organization.search(page_number, records_per_page)
        page['is_paginated'] = True if total_pages > 1 else False
        page['current_page'] = page_number + 1
        page['next'] = page_number + 2
        page['prev'] = page_number
        page['last_page'] = total_pages
        return render(request, 'pages/organization/list.html', {'organization_list': organization_list,
                                                                'page': page})

    @is_super_admin
    def update(self, request):
        organization_id = request.GET['organization_id']
        if request.GET.get('page'):
            page_number = request.GET.get('page')
        else:
            page_number = '1'
        organization = Organization.get_by_id(organization_id)
        user_mail = organization.contact_email
        users = User.get_by_email(user_mail)
        if request.method == 'GET':
            return render(request, 'pages/organization/update.html', {"organization": organization, "users": users,
                                                                      'page': page_number})
        elif request.method == 'POST':
            request_dict = {x: request.POST.get(x) for x in request.POST.keys()}
            created_by = request.session['user_id']
            organization_dict = {}
            organization_dict['legal_name'] = request.POST.get('legal_name')
            organization_dict['id'] = request.POST.get('organization_id')
            organization_dict['slug'] = slugify(request.POST.get('legal_name'))
            organization_dict['created_by'] = created_by
            organization_dict['display_name'] = request.POST.get('display_name')
            organization_dict['contact_email'] = request.POST.get('contact_email')
            organization_dict['contact_phone'] = request.POST.get('contact_phone')
            organization_dict['contact_address'] = request.POST.get('contact_address')
            organization_dict['city'] = request.POST.get('city')
            try:
                # if users is not None:
                #     messages.add_message(request, messages.ERROR, "An account with this email already exist.")
                #     return render(request, 'pages/organization/update.html', request_dict)

                organization.update_legal_name(organization_dict['legal_name'])
                organization.update_display_name(organization_dict['display_name'])
                organization.update_contact_phone(organization_dict['contact_phone'])
                organization.update_city(organization_dict['city'])
                organization.update_contact_address(organization_dict['contact_address'])
                organization.update_contact_email(organization_dict['contact_email'])

                user_dict = {}
                user_dict['first_name'] = request.POST.get('first_name')
                user_dict['last_name'] = request.POST.get('last_name')
                user_dict['email'] = organization_dict['contact_email']
                user_dict['mobile_number'] = organization_dict['contact_phone']

                users.update_first_name(user_dict['first_name'])
                users.update_last_name(user_dict['last_name'])
                users.update_email(user_dict['email'])
                users.update_mobile_number(user_dict['mobile_number'])

                msg_string = request_dict['legal_name'] + " updated successfully"
            except AssertionError as error:
                messages.add_message(request, messages.ERROR, error)
                return render(request, 'pages/organization/update.html', request_dict)
            # except Exception as error:
            #     print(error)
            #     if isinstance(error, IntegrityError):
            #         messages.add_message(request, messages.ERROR, "Organization already exists.")
            #     else:
            #         messages.add_message(request, messages.ERROR, "Something went wrong")
            #     return render(request, 'pages/organization/update.html', request_dict)
            # try:
            #     html_message = render_to_string('pages/email/email-add-organisation.html',
            #                                     {'organization': organization,
            #                                      'password': password})
            #     subject = "You have been on-boarded to My Candidature"
            #     recipient_list = [organization.contact_email]
            #
            #     send_email.delay(subject=subject, message='', from_email=FROM_EMAIL,
            #                      recipient_list=recipient_list,
            #                      fail_silently=False, html_message=html_message)
            except:
                msg_string = request_dict['legal_name'] + " updated successfully, could not send email."
            messages.success(request, msg_string)
            return HttpResponseRedirect('/organization/list?page=' + str(page_number))

    @is_super_admin
    def view(self, request):
        organization_id = request.GET['organization_id']
        if request.GET.get('page'):
            page_number = request.GET.get('page')
        else:
            page_number = '1'
        organization = Organization.get_by_id(organization_id)
        user_mail = organization.contact_email
        users = User.get_by_email(user_mail)
        return TemplateResponse(request, 'pages/organization/view.html',
                                {'organization': organization, 'users': users,
                                 'page': page_number})

    @is_super_admin
    def delete_org(self, request):
        organization_id = request.GET['organization_id']
        if request.GET.get('page'):
            page_number = request.GET.get('page')
        else:
            page_number = '1'
        organization = Organization.get_by_id(organization_id)
        email = organization.contact_email
        user = User.get_by_email(email)
        org_id = organization_id
        user_role = UserRole.get_by_org_id(org_id)
        Test.delete_by_org_id(org_id)
        Result.delete_by_org_id(org_id)
        user.delete()
        user_role.delete()
        organization.delete()
        return HttpResponseRedirect('/organization/list?page=' + str(page_number))

    @is_super_admin
    def deactivate_org(self, request):
        organization_id = request.GET['organization_id']
        if request.GET.get('page'):
            page_number = request.GET.get('page')
        else:
            page_number = '1'
        organization = Organization.get_by_id(organization_id)
        user_mail = organization.contact_email
        users = User.get_by_email(user_mail)
        User.deactivate(users)
        Organization.deactivate(organization)
        messages.add_message(request, messages.SUCCESS, 'Organization deactivated successfully.')
        return HttpResponseRedirect('/organization/list?page=' + str(page_number))

    @is_super_admin
    def activate_org(self, request):
        organization_id = request.GET['organization_id']
        if request.GET.get('page'):
            page_number = request.GET.get('page')
        else:
            page_number = '1'
        organization = Organization.get_by_id(organization_id)
        user_mail = organization.contact_email
        users = User.get_by_email(user_mail)
        User.activate(users)
        Organization.activate(organization)
        messages.add_message(request, messages.SUCCESS, 'Organization activated successfully.')
        return HttpResponseRedirect('/organization/list?page=' + str(page_number))

    @is_super_admin
    def reset_password(cls, request: HttpRequest):
        organization_id = request.GET.get('organization_id')
        if request.GET.get('page'):
            page_number = request.GET.get('page')
        else:
            page_number = '1'
        organization = Organization.get_by_id(organization_id)
        user = User.get_by_email(organization.contact_email)
        password = User._generate_random_password(6)
        User.update_password(user, password)
        url = CONFIG['BASE_URL'] + '/user/login'
        try:
            html_message = render_to_string('pages/email/email-reset-password-organization.html',
                                            {'organization': organization,
                                             'password': password,
                                             'url': url})
            subject = 'Your Password has been reset'
            recipient_list = [organization.contact_email]
            send_email.delay(subject = subject, message = '', from_email = FROM_EMAIL,
                             recipient_list = recipient_list,
                             fail_silently = False, html_message = html_message)
            messages.success(request, 'Password reset mail sent')

        except Exception as e:
            msg_string = 'Password reset successfully, could not send email. Try Again'
            messages.error(request, msg_string)
        return HttpResponseRedirect('/organization/list?page=' + str(page_number))

    @classmethod
    def search(cls, request: HttpRequest):
        page = {'current_page': None,
                'next': None,
                'prev': None,
                'last_page': None,
                'stages': 2,
                'is_paginated': False
                }
        try:
            page_number = request.GET.get('page')
            page_number = int(page_number)
            if page_number < 1:
                page_number = 1
            page_number -= 1
        except Exception:
            page_number = 0
        if request.is_ajax():
            query = request.POST.get('query', None)

            if query is None or query == '':
                records_per_page = 10

                total = Organization.get_total()
                total_pages = math.ceil((total / records_per_page))

                if page_number >= total_pages:
                    page_number = 0
                organization_list = Organization.search(page_number, records_per_page)
                page['is_paginated'] = True if total_pages > 1 else False
                page['current_page'] = page_number + 1
                page['next'] = page_number + 2
                page['prev'] = page_number
                page['last_page'] = total_pages

            else:
                query = query.strip()
                query = query.replace(" ", ' | ')
                organization_list = Organization._get_search_results(query)
                page['is_paginated'] = False

            flag = False
            if len(organization_list):
                flag = True

            organizations = render_to_string('pages/organization/table.html', {'organization_list': organization_list,
                                                                               'page': page})
            page = render_to_string('pages/pagination.html', {'page': page})
            if flag:
                data = {
                    'organizations': organizations,
                    'page': page
                }
            else:
                data = {
                    'organizations': None,
                    'page': None
                }
            return JsonResponse(data)
