import base64
import os
import random
from datetime import datetime
import math

import jwt
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpRequest, HttpResponse, HttpResponseNotFound, JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from ipify import get_ip
from django.template.response import TemplateResponse
from jwt import InvalidSignatureError

from apps.main.models.base import session
from apps.main.models.organization import Organization
from apps.main.models.question import Question
from apps.main.models.question_difficulty import QuestionDifficulty, DifficultyLevel, DifficultySeconds
from apps.main.models.question_skill import QuestionSkill
from apps.main.models.result import Result
from apps.main.models.skill import Skill
from apps.main.models.test import Test, Status
from apps.main.models.user import User, Gender
from apps.main.models.user_role import UserRole, RoleType
from apps.main.tasks.send_email import send_email
from apps.main.views.base_view import BaseView, is_on_chrome_desktop
from my_candidature.settings import SECRET_KEY, CONFIG, FROM_EMAIL, HASHIDS_SALT
from hashids import Hashids


# from requests import get

class TestView(BaseView):

    @classmethod
    def get_skills_active(cls):
        # get all skills
        skills = session.query(Skill).all()
        # get all questions
        questions = session.query(Question).all()
        # get all active questions
        questions = [question.id for question in questions if question.is_active]
        # get all questions with skills
        question_skills = session.query(QuestionSkill).all()
        # get all questions from questions_skills that are active
        question_skills = [question for question in question_skills if question.question_id in questions]
        # get all ids with active questions and more than 0 questions
        question_skills = [question.skill_id for question in question_skills]
        # get unique skills
        question_skills = list(set(question_skills))
        # filter skills to show from all skills
        skills = [skill for skill in skills if skill.id in question_skills]

        return skills

    @staticmethod
    def add(request: HttpRequest):
        if request.method == 'GET':
            skills = TestView.get_skills_active()
            gender = list(Gender)
            return render(request, 'pages/test/add.html', {'skills': skills,
                                                           'gender': gender,
                                                           'skill_length': len(skills)})

    @classmethod
    def generate(cls, request):
        if request.method == 'POST':
            total_questions = request.POST.get('total_questions')
            gender = request.POST.get('gender')
            created_by = request.session['user_id']
            expire_at = request.POST.get('test_expire')
            designation = request.POST.get('designation').strip()
            try:
                expire_at = datetime.strptime(expire_at, '%Y-%m-%d %H:%M')
            except Exception as e:
                messages.error(request, 'Can\'t Add test. Illegal Date format')
                return HttpResponseRedirect('/test/list')
            # adding validator to not add expiry date in past
            if expire_at < datetime.now():
                messages.error(request, 'Can\'t add test. Expiry time can not be before current time.')
                return HttpResponseRedirect('/test/add')

            org_id = request.session['user_org_id']

            index = 1
            skill_index = 'skill_' + str(index)
            experience_index = 'experience_' + str(index)
            final_skill_exp_list = []
            while skill_index in request.POST:
                skill_exp_dict = {
                    'skill': request.POST.get(skill_index),
                    'experience': request.POST.get(experience_index)
                }
                final_skill_exp_list.append(skill_exp_dict.copy())
                index = index + 1
                skill_index = 'skill_' + str(index)
                experience_index = 'experience_' + str(index)

            email = request.POST.get('email')
            skills = []
            experiences = []
            for f in final_skill_exp_list:
                skills.append(f['skill'])
                experiences.append(f['experience'])
            user = User.get_by_email(email)
            if user is None:
                user_dict = {
                    'first_name': request.POST.get('first_name'),
                    'last_name': request.POST.get('last_name'),
                    'email': email,
                    'mobile_number': request.POST.get('mobile_number'),
                    'gender': int(gender),
                    'created_by': created_by,
                    'password': None
                }

                user, password = User.add(user_dict)
                user_role_dict = {
                    'role_id': RoleType.CANDIDATE,
                    'user_id': user.id,
                    'org_id': org_id
                }
                user_role = UserRole.add(user_role_dict)

            # fetched_questions = cls.optimized_random(total_questions)
            fetched_questions, total_time = cls.get_qs_by_experience(int(total_questions), skills, experiences)

            if len(fetched_questions) < int(total_questions):
                messages.add_message(request, messages.ERROR,
                                     'Can\'t Add Test, the skill does not have sufficient questions for the test, kindly add more questions to the skill.')
                return HttpResponseRedirect('/test/list')

            questions = []

            for f in fetched_questions:
                qs_dict = {'id': f}

                questions.append(qs_dict.copy())

            test_dict = {
                'candidate_id': user.id,
                'total_questions': total_questions,
                'org_id': org_id,
                'skill': final_skill_exp_list,
                'question_ids': questions,
                'total_time': total_time,
                'time_left': total_time,
                'status': Status.NEW,
                'created_by': created_by,
                'updated_at': None,
                'expire_at': expire_at,
                'designation': designation
            }
            test = Test.add(test_dict)
            test_token = TestView.generate_test_token(test.candidate_id, test.id, test.expire_at)
            test.add_token(test_token)
            # Send emails to candidate
            TestView.send_email_to_candidate(request, expire_at, test)
            return HttpResponseRedirect('/test/list')

    @classmethod
    def send_email_to_candidate(cls, request, expire_at, test):
        candidate = User.get_by_id(test.candidate_id)
        organization = Organization.get_by_id(test.org_id)
        user = User.get_by_email(organization.contact_email)
        msg_string = "Test created successfully."
        try:
            url = CONFIG['BASE_URL'] + "/test/start?token=" + str(test.token)
            subject = 'You are invited by ' + organization.display_name
            message = ''
            recipient_list = [candidate.email]
            fail_silently = False
            html_message = render_to_string('pages/email/email-candidate-add-test.html',
                                            {'url': url, 'name': candidate.first_name,
                                             'time': expire_at.strftime('%d, %b %Y %I:%M %p'),
                                             'organisation_name': organization.display_name,
                                             'designation': test.designation,
                                             'user': User._full_name(user)
                                             })
            send_email.delay(subject, message, FROM_EMAIL, recipient_list,
                             fail_silently = fail_silently, html_message = html_message)
        except Exception as error:
            msg_string = "Test created successfully, could not send email."

        messages.add_message(request, messages.SUCCESS, msg_string)

    @classmethod
    def get_qs_by_experience(cls, total_questions, skills, experiences):
        questions_from_skills = TestView.get_qs_by_skills(skills)
        question_ids = []
        for qs_list in questions_from_skills:
            index_question_ids = []
            for q in qs_list:
                index_question_ids.append(q.id)
            question_ids.append(index_question_ids)
        questions = []
        processed_questions = {}
        for index, qs_list in enumerate(question_ids):
            retrieved_questions = QuestionDifficulty.get_qs_by_experience(qs_list, experiences[index])
            for qs_difficulty in retrieved_questions:
                qs_id = qs_difficulty.question_id
                diff_level = qs_difficulty.difficulty_level
                if qs_id not in processed_questions.keys():
                    processed_questions[qs_id] = diff_level
                else:
                    if processed_questions[qs_id].value > diff_level.value:
                        processed_questions[qs_id] = diff_level
            questions.append(retrieved_questions)

        final_qs_ids = []
        total_time = 0
        processed_questions_keys = [key for key, value in processed_questions.items()]
        _questions = {}
        if len(processed_questions_keys) > total_questions:
            random.shuffle(processed_questions_keys)
            processed_questions_keys = processed_questions_keys[:total_questions]
            for key in processed_questions_keys:
                _questions[key] = processed_questions[key]
        else:
            _questions = processed_questions

        for key, val in _questions.items():
            if processed_questions[key].value == DifficultyLevel.EASY.value:
                total_time = total_time + DifficultySeconds.EASY.value
            if processed_questions[key].value == DifficultyLevel.MEDIUM.value:
                total_time = total_time + DifficultySeconds.MEDIUM.value
            if processed_questions[key].value == DifficultyLevel.HARD.value:
                total_time = total_time + DifficultySeconds.HARD.value
        # for qs_list in questions:
        #     for q in qs_list:
        #         final_qs_ids.append(q.question_id)
        # unique_qs_ids = set(final_qs_ids)
        # unique_final_qs_ids = list(unique_qs_ids)
        #
        # # In-place shuffle array
        # random.shuffle(unique_final_qs_ids)
        unique_final_qs_ids = list(processed_questions.keys())
        return unique_final_qs_ids[:total_questions], total_time

    @classmethod
    def get_qs_by_skills(cls, skills):
        skills_len = len(skills)
        questions_skills = []
        i = 0
        while i < skills_len:
            skill = Skill.get_by_id(skills[i])
            # Fetch questions based on skills
            retrieved_questions = QuestionSkill.get_active_questions_by_skill(skill.id)
            questions_skills.append(retrieved_questions)
            i = i + 1
        return questions_skills

    @classmethod
    def thankyou(cls, request: HttpRequest):
        _hashids = Hashids(salt = HASHIDS_SALT, min_length = 5)
        test_id = None
        try:
            test_hash = request.GET.get('f_id')
            test_id = int(_hashids.decode(test_hash)[0])
            test = Test.get_by_id(test_id)
            if len(test.candidate_feedback) != 0:
                test_id = None
        except Exception as e:
            pass
        return render(request, 'pages/test/thankyou.html', {'test_id': test_id})

    @classmethod
    def expired(cls, request):
        token = request.session.get('token')
        token_data = jwt.decode(token, SECRET_KEY)
        test_id = token_data['test_id']
        test = Test.get_by_id(test_id)
        organization = Organization.get_by_id(test.org_id)
        return render(request, 'pages/test/expired.html',
                      {'name': organization.display_name, 'email': organization.contact_email})

    @classmethod
    def list(cls, request):
        org_id = request.session.get('user_org_id')

        tests = Test._get_new_tests()
        for test in tests:
            try:
                if test.expire_at < datetime.now():
                    test.update_status(Status.EXPIRED)
            except Exception as e:
                pass

        page = {'current_page': None,
                'next': None,
                'prev': None,
                'last_page': None,
                'stages': 2,
                'is_paginated': False
                }
        records_per_page = 10
        try:
            page_number = request.GET.get('page')
            page_number = int(page_number)
            if page_number < 1:
                page_number = 1
            page_number -= 1
        except Exception:
            page_number = 0

        total = Test.get_total(org_id)
        total_pages = math.ceil((total / records_per_page))
        if page_number >= total_pages:
            page_number = 0
        test_list = Test.search(page_number, records_per_page, org_id)
        page['is_paginated'] = True if total_pages > 1 else False
        page['current_page'] = page_number + 1
        page['next'] = page_number + 2
        page['prev'] = page_number
        page['last_page'] = total_pages

        skill_ids = []
        user_list = []
        user_name = []
        user_email = []
        i = 0
        for test in test_list:
            user_list.append(User.get_by_id(test.candidate_id))
            try:
                user_name.append(user_list[i].first_name)
                user_email.append(user_list[i].email)
            except Exception:
                pass
            i += 1
            test_skills = test.skill
            for _test_skill in test_skills:
                skill_ids.append(int(_test_skill['skill']))

        skill_data = Skill.get_by_ids(skill_ids)

        skill_dict = {}
        for id, name in skill_data:
            skill_dict.setdefault(id, []).append(name)

        return render(request, 'pages/test/list.html',
                      {'test_list': test_list, 'skill_dict': skill_dict,
                       'base_url': CONFIG['BASE_URL'], 'user_name': user_name,
                       'user_email': user_email, 'page': page})

    @classmethod
    def update_timer(cls, request):
        time_left = request.POST.get('time_left')
        test_id = request.POST.get('test_id')
        test = Test.get_by_id(test_id)
        organization = Organization.get_by_id(test.org_id)
        candidate = User.get_by_id(test.candidate_id)
        test_status = test.status
        total_time = test.total_time
        test.update_left_time(time_left)
        # Will only run the SQL query once when time_left <= 0
        if int(time_left) <= 0:
            status = Status.CLOSED
            test.update_status(status)
            # SEND EMAIL
            subject = candidate.first_name + ' ' + candidate.last_name + ' has completed their test.'
            message = ''
            recipient_list = [organization.contact_email]
            fail_silently = False
            # Send email to organization
            html_message = render_to_string('pages/email/email-organization-end-test.html',
                                            {'test': test, 'candidate': candidate})
            send_email.delay(subject, message, FROM_EMAIL, recipient_list,
                             fail_silently = fail_silently, html_message = html_message)
            # Send email to candidate
            subject = 'Thank you for submitting the test.'
            recipient_list = [candidate.email]
            html_message = render_to_string('pages/email/email-candidate-end-test.html', {})
            send_email.delay(subject, message, FROM_EMAIL, recipient_list,
                             fail_silently = fail_silently, html_message = html_message)
        # Will only run the SQL query once if it's not been put IN_PROGRESS yet
        elif int(time_left) < total_time:
            if test_status == Status.NEW:
                status = Status.IN_PROGRESS
                test.update_status(status)

        return HttpResponse("Test timer's being updated successfully!")

    @staticmethod
    def generate_test_token(candidate_id, test_id, expire_at):
        secret = SECRET_KEY
        data = {
            'candidate_id': candidate_id,
            'test_id': test_id,
            'expire_date': str(expire_at)
        }
        token = jwt.encode(data, secret)
        token_string = token.decode('utf-8')
        return token_string

    @classmethod
    @is_on_chrome_desktop
    def start(cls, request: HttpRequest):
        token = request.GET.get('token')
        _hashids = Hashids(salt = HASHIDS_SALT, min_length = 5)
        # Make directory for storing candidate face images if not already made
        if os.path.exists("media"):
            if os.path.exists("media/test"):
                pass
            else:
                os.mkdir('media/test')
        else:
            os.mkdir('media')
            os.mkdir('media/test')
        if cls.is_valid(token):
            token_data = jwt.decode(token, SECRET_KEY)
            test_id, candidate_id = token_data['test_id'], token_data['candidate_id']
            test = session.query(Test).filter(Test.id == test_id).scalar()
            try:
                if test.status == Status.EXPIRED or test.expire_at < datetime.now():
                    test.update_status(Status.EXPIRED)
                    request.session['token'] = token
                    return HttpResponseRedirect('/test/expired')
            except Exception:
                pass
            # Redirect to thank-you page if timer's over
            if test.time_left <= 0 or test.status == Status.CLOSED:
                return HttpResponseRedirect('/test/thankyou')

            question_id = None
            if test.question_ids:
                question_id = test.question_ids[0]
                skill_ids = []
                for s in test.skill:
                    skill_ids.append(s['skill'])
                skills = []
                for s_id in skill_ids:
                    skill = Skill.get_by_id(s_id)
                    skills.append(skill.title)

                result = Result.get_by_test(test_id)

                # Get public IP using ipify
                ip_address = None
                while not ip_address:
                    ip_address = get_ip()
                # IPIFY is SLOWING the site
                # ip_address = '55.66.66.77'
                ip_address_list = test.ip_address_list if test.ip_address_list != None else []
                if test.status == Status.NEW:
                    if ip_address_list is None:
                        test.save_ip_address(ip_address)
                    elif ip_address_list is not None:
                        if ip_address not in ip_address_list:
                            test.save_ip_address(ip_address)
                elif ip_address not in ip_address_list:
                    test.save_ip_address(ip_address)

                if result is not None:
                    pending_questions = min(test.total_questions, len(test.question_ids)) - len(result.answers)
                    pending_questions_percent = (pending_questions / min(test.total_questions,
                                                                         len(test.question_ids))) * 100
                else:
                    pending_questions_percent = 100

                hashed_question_id = _hashids.encode(question_id['id'])
                data = {
                    'token': token,
                    'question_id': hashed_question_id,
                    'test': test,
                    'skills': skills,
                    'result': result,
                    'pending_questions_percent': pending_questions_percent,
                    'ip_address': ip_address
                }
                # data = TestView.get_question_data(question, token, next_question_id, on_last_question, time_left,
                #                                   test_id)

                return render(request, 'pages/test/start.html', data)
        return render(request, 'layouts/404.html', {})

    @classmethod
    @is_on_chrome_desktop
    def inprogress(cls, request: HttpRequest):
        token = request.GET.get('token')
        _hashids = Hashids(salt = HASHIDS_SALT, min_length = 5)
        if cls.is_valid(token):
            token_data = jwt.decode(token, SECRET_KEY)
            test_id, candidate_id = token_data['test_id'], token_data['candidate_id']
            test = session.query(Test).filter(Test.id == test_id).scalar()
            organization = Organization.get_by_id(test.org_id)
            candidate = User.get_by_id(test.candidate_id)
            # Redirect to thank-you page if timer's over
            if test.time_left <= 0 or test.status == Status.CLOSED:
                return HttpResponseRedirect('/test/thankyou')

            time_left = test.time_left
            question_ids = []
            data = None
            if test.question_ids:
                for _item in test.question_ids:
                    question_ids.append(_item['id'])
            # Render 1st question & handle start_test's POST request & upload candidate image
            if 'candidate_photo' in request.POST:
                # If the candidate hasn't captured their photo
                if request.POST['candidate_photo'] == '':
                    messages.add_message(request, messages.ERROR,
                                         "Please submit your photo first.")
                    # redirect to the same page it came from with the error message above
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
                # url starts with 'data:image/png;base64,myimagestringishere'
                image_data_url = request.POST['candidate_photo']
                # [22:] because till index 21 we don't have our string as shown in above comment
                image_string = image_data_url[22:]
                image_data = base64.b64decode(image_string)

                image_extension = '.png'
                media_directory = 'media/test/'
                image_name = candidate.first_name + '_' + candidate.last_name + '_' + str(test.id) + '_' + str(
                    candidate.id) + image_extension
                image_path = media_directory + image_name
                test.save_candidate_image(image_name)

                with open(image_path, 'wb') as f:
                    f.write(image_data)

                if test.status.value == Status.NEW.value:
                    status = Status.IN_PROGRESS
                    test.update_status(status)
                    subject = 'Test started by ' + candidate.first_name + ' ' + candidate.last_name
                    message = ''
                    from_email = FROM_EMAIL
                    recipient_list = [organization.contact_email]
                    fail_silently = False
                    html_message = render_to_string('pages/email/email-organization-start-test.html',
                                                    {'test': test, 'candidate': candidate})
                    send_email.delay(subject, message, from_email, recipient_list,
                                     fail_silently = fail_silently, html_message = html_message)

                current_question_id = question_ids[0]
                on_last_question = False
                next_question_id = _hashids.encode(question_ids[1])
                # next_question_id = current_question_id + 1

                question = session.query(Question).filter(Question.id == current_question_id).scalar()
                time_left = test.time_left
                data = cls.get_question_data(question, token, next_question_id, on_last_question, time_left,
                                             test_id)
                # Display Question's data on page
                data['question_number'] = 1
                return render(request, 'pages/test/inprogress.html', data)

            # If user is directly going to the URL from Qs ID
            if 'next_question_id' in request.GET:
                if request.method == 'GET':
                    if test.status.value == Status.NEW.value:
                        status = Status.IN_PROGRESS
                        test.update_status(status)
                        subject = 'Test started by ' + candidate.first_name + ' ' + candidate.last_name
                        message = ''
                        from_email = FROM_EMAIL
                        recipient_list = [organization.contact_email]
                        fail_silently = False
                        html_message = render_to_string('pages/email/email-organization-start-test.html',
                                                        {'test': test, 'candidate': candidate})
                        send_email.delay(subject, message, from_email, recipient_list,
                                         fail_silently = fail_silently, html_message = html_message)

                    question_id = request.GET.get('next_question_id')
                    try:
                        question_id = int(_hashids.decode(question_id)[0])
                    except Exception as e:
                        return HttpResponseNotFound("Oops. How did you land here?")

                    try:
                        current_qs_index = question_ids.index(question_id)
                    except ValueError:
                        return HttpResponseNotFound("Oops. How did you land here?")
                    # If it's last Qs ID (user could directly go to last Qs from its URL)
                    if question_id == question_ids[-1]:
                        on_last_question = True
                        next_question_id = -1
                    # If we're not on last Qs
                    else:
                        on_last_question = False
                        next_question_id = _hashids.encode(question_ids[current_qs_index + 1])
                    question = session.query(Question).filter(Question.id == question_id).scalar()
                    data = cls.get_question_data(question, token, next_question_id, on_last_question, time_left,
                                                 test_id)
                    # Display Question's data on page
                    data['question_number'] = current_qs_index + 1
                    return render(request, 'pages/test/inprogress.html', data)

                # Handle last question's submit event (Handle last Qs POST data)
                elif request.GET.get('next_question_id') == '-1' and request.method == 'POST':
                    cls.handle_question_submit(request.POST, test_id, candidate_id)
                    test.update_status(Status.CLOSED)
                    # TODO: Send mail (to org admin & candidate - on test completion)
                    subject = candidate.first_name + ' ' + candidate.last_name + ' has completed their test.'
                    message = ''
                    recipient_list = [organization.contact_email]
                    fail_silently = False
                    # Send email to organization
                    html_message = render_to_string('pages/email/email-organization-end-test.html',
                                                    {'test': test, 'candidate': candidate})
                    send_email.delay(subject, message, FROM_EMAIL, recipient_list,
                                     fail_silently = fail_silently, html_message = html_message)
                    # Send email to candidate
                    subject = 'Thank you for submitting the test.'
                    recipient_list = [candidate.email]
                    html_message = render_to_string('pages/email/email-candidate-end-test.html', {})
                    send_email.delay(subject, message, FROM_EMAIL, recipient_list,
                                     fail_silently = fail_silently, html_message = html_message)
                    test_hash = _hashids.encode(test.id)
                    return HttpResponseRedirect('/test/thankyou?f_id='+test_hash)

            if 'question_id' in request.POST:
                # 'question_id' from hidden input
                current_question_id = int(request.POST.get('question_id'))
                total_questions = min(test.total_questions, len(test.question_ids))
                next_question_id = None
                on_last_question = False
                cls.handle_question_submit(request.POST, test_id, candidate_id)
                current_qs_index = question_ids.index(current_question_id)
                current_question_id = question_ids[current_qs_index + 1]
                current_qs_index = question_ids.index(current_question_id)
                if current_qs_index == total_questions - 1:
                    on_last_question = True
                time_left = test.time_left
                if on_last_question:
                    question = session.query(Question).filter(Question.id == current_question_id).scalar()
                    data = cls.get_question_data(question, token, next_question_id, on_last_question, time_left,
                                                 test_id)
                    data['question_number'] = current_qs_index + 1
                    return render(request, 'pages/test/inprogress.html', data)
                else:
                    next_question_id = _hashids.encode(question_ids[current_qs_index + 1])
                    question = session.query(Question).filter(Question.id == current_question_id).scalar()
                    data = cls.get_question_data(question, token, next_question_id, on_last_question, time_left,
                                                 test_id)
                    data['question_number'] = current_qs_index + 1
                    return render(request, 'pages/test/inprogress.html', data)

    @staticmethod
    def get_question_data(question, token, next_question_id, on_last_question, time_left, test_id):
        question_type = question.type.value
        # [0] to get a list rather than tuple
        all_answers = Result.get_answers_by_test_id(test_id)
        if all_answers is not None:
            all_answers = all_answers[0]
        current_qs_id = question.id
        data = {
            'question': {
                'id': question.id,
                'title': question.title,
                'answer_options': question.answer_options,
                'type': question_type,
                'description': question.description,
                'previous_answer': None,
                'description_image': question.description_image
            },
            'token': token,
            'next_question_id': next_question_id,
            'on_last_question': on_last_question,
            'time_left': time_left,
            'test_id': test_id,
        }
        if all_answers is not None:
            for index, value in enumerate(all_answers):
                if value['question_id'] == current_qs_id:
                    data['question']['previous_answer'] = value['answers']

        return data

    @staticmethod
    def handle_question_submit(post_data, test_id, candidate_id):
        # 'question_id' from hidden input
        current_question_id = int(post_data.get('question_id'))
        test = Test.get_by_id(test_id)
        org_id = test.org_id

        time_left = None
        if post_data['time_left'] != '':
            time_left = int(post_data.get('time_left'))

        if 'description' in post_data:
            answers_dict = post_data.get('description')
        elif 'answer_options' in post_data:
            answers_list = post_data.getlist('answer_options')
            answers_dict = dict((value, True) for value in answers_list)
        answers = {
            'question_id': current_question_id,
            'answers': answers_dict
        }
        result_dict = {
            'test_id': test_id,
            'candidate_id': candidate_id,
            'org_id': org_id,
            'answers': answers,
            'score': 0
        }
        # Save response in 'result'
        TestView.save_result(result_dict, test_id)
        # Save timer in test table
        if time_left is None:
            time_left = test.time_left
        test.update_left_time(time_left)

    @staticmethod
    def save_result(result_dict, test_id):
        result = session.query(Result).filter(Result.test_id == test_id).first()
        if result is None:
            result_obj = Result()
            result_obj.add(result_dict)
        else:
            result.update_answers(result_dict)

    @staticmethod
    def is_valid(token):
        try:
            jwt.decode(token, SECRET_KEY)
            return True
        except InvalidSignatureError:
            return False

    @staticmethod
    def use_chrome(request):
        return render(request, 'pages/test/use_chrome.html', {})

    @staticmethod
    def view(request):
        test_id = request.GET['test_id']
        test = Test.get_by_id(test_id)
        users = User.get_by_id(test.candidate_id)
        skills = test.skill
        skill_length = len(skills)
        s = []
        e = []
        length = 0
        for i in range(skill_length):
            skill_id = test.skill[i]['skill']
            skills = Skill.get_by_id(skill_id)
            exp = test.skill[i]['experience']
            s.append(skills.title)
            e.append(exp)
            length += 1
        return TemplateResponse(request, 'pages/test/view.html', {"test": test,
                                                                  'skills': skills,
                                                                  's': s,
                                                                  'e': e,
                                                                  'length': length,
                                                                  'users': users})

    @classmethod
    def update(cls, request):
        test_id = request.GET['test_id']
        test = Test.get_by_id(test_id)
        user_object = User.get_by_id(test.candidate_id)
        gender_list = list(Gender)
        if request.method == 'GET':
            all_skills = cls.get_skills_active()

            expiry = str(test.expire_at)
            expiry = expiry[0:10] + 'T' + expiry[11:16]

            all_skills_id = [skill.id for skill in all_skills]
            test_skills_all = test.skill
            # test_skills is the test skills intersection active skills based on questions
            # but here we are getting out all of them
            test_skills = [int(tes['skill']) for tes in test_skills_all]
            # test_experience is the test experience intersection active skills based on questions
            # but here we are getting out all of them
            test_experience = [int(tes['experience']) for tes in test_skills_all]

            # performing test skills and experience intersection all_skills_ids based on active questions
            for t in test_skills:
                if t not in all_skills_id:
                    i = test_skills.index(t)
                    test_skills.remove(test_skills[i])
                    test_experience.remove((test_experience[i]))

            length = len(test_skills) if len(test_skills) else 1
            return render(request, 'pages/test/update.html', {"test": test,
                                                              "userObject": user_object,
                                                              'length': length,
                                                              'all_skills': all_skills,
                                                              'skill_length': len(all_skills),
                                                              'gender_list': gender_list,
                                                              'expiry': expiry,
                                                              'test_skills': test_skills,
                                                              'test_experience': test_experience
                                                              })

        elif request.method == 'POST':
            org_id = request.session['user_org_id']
            gender = request.POST.get('gender')
            total_questions = request.POST.get('total_questions')
            designation = request.POST.get('designation').strip()
            expire_at = request.POST.get('test_expire')
            try:
                if test.expire_at < datetime.now():
                    test.update_status(Status.EXPIRED)
                    messages.error(request, 'Can\'t update test. Test Expired')
                    return HttpResponseRedirect('/test/list')
            except Exception:
                pass
            if expire_at == '':
                messages.error(request, "Couldn't update test. Please try again with a valid date and time.")
                return HttpResponseRedirect('/test/list')
            try:
                expire_at = datetime.strptime(expire_at, '%Y-%m-%d %H:%M')
            except Exception as e:
                messages.error(request, 'Can\'t update test. Illegal Date format')
                return HttpResponseRedirect('/test/list')
            # adding validator to not update date in past
            if expire_at < datetime.now():
                messages.error(request, 'Can\'t update test. Expiry time can not be before current time.')
                return HttpResponseRedirect('/test/list')

            index = 1
            skill_index = 'skill_' + str(index)
            experience_index = 'experience_' + str(index)
            final_skill_exp_list = []
            while skill_index in request.POST:
                skill_exp_dict = {
                    'skill': request.POST.get(skill_index),
                    'experience': request.POST.get(experience_index)
                }
                final_skill_exp_list.append(skill_exp_dict.copy())
                index = index + 1
                skill_index = 'skill_' + str(index)
                experience_index = 'experience_' + str(index)
            skills = []
            experiences = []
            for f in final_skill_exp_list:
                skills.append(f['skill'])
                experiences.append(f['experience'])
            if user_object is not None:
                user_dict = {
                    'first_name': request.POST.get('first_name'),
                    'last_name': request.POST.get('last_name'),
                    'email': request.POST.get('email'),
                    'mobile_number': request.POST.get('mobile_number'),
                    'gender': int(gender),
                    'password': None
                }
                user_object.update_first_name(user_dict['first_name'])
                user_object.update_last_name(user_dict['last_name'])
                user_object.update_email(user_dict['email'])
                user_object.update_mobile_number(user_dict['mobile_number'])
                user_object.update_gender(user_dict['gender'])
                # user_object, password = User.add(user_dict)
                user_role_dict = {
                    'role_id': RoleType.CANDIDATE,
                    'user_id': user_object.id,
                    'org_id': org_id
                }
                # user_role = UserRole.add(user_role_dict)

            # fetched_questions = cls.optimized_random(total_questions)
            fetched_questions, total_time = cls.get_qs_by_experience(int(total_questions), skills, experiences)

            if (len(fetched_questions) < int(total_questions)):
                messages.add_message(request, messages.ERROR,
                                     'Can\'t Update Test, the skill does not have sufficient questions for the test, kindly add more questions to the skill.')
                return HttpResponseRedirect('/test/list')

            questions = []

            for f in fetched_questions:
                qs_dict = {'id': f}

                questions.append(qs_dict.copy())

            # test_dict = {
            #     'candidate_id': user.id,
            #     'total_questions': total_questions,
            #     'org_id': org_id,
            #     'skill': final_skill_exp_list,
            #     'question_ids': questions,
            #     'total_time': total_time,
            #     'time_left': total_time,
            #     'status': Status.NEW,
            #     'created_by': created_by,
            #     'updated_at': None
            # }

            test.update_total_questions(total_questions)
            test.update_skill(final_skill_exp_list)
            test.update_total_time(total_time)
            test.update_time_left(total_time)
            test.update_expire_date(expire_at)
            test.update_designation(designation)
            # Send emails to candidate
            msg_string = "Test updated successfully."
            # try:
            #     url = CONFIG['BASE_URL'] + "test/start?token=" + str(test.token)
            #     subject = 'You are invited by ' + organization.display_name
            #     message = ''
            #     recipient_list = [candidate.email]
            #     fail_silently = False
            #     html_message = render_to_string('pages/email/email-candidate-add-test.html',
            #                                     {'url': url, 'name': candidate.first_name})
            #     send_email.delay(subject, message, FROM_EMAIL, recipient_list,
            #                      fail_silently=fail_silently, html_message=html_message)
            # except Exception as error:
            #     msg_string = "Test created successfully, could not send email."
            messages.add_message(request, messages.SUCCESS, msg_string)
            return HttpResponseRedirect('/test/list')

    @classmethod
    def resend_email(cls, request: HttpRequest):
        if request.method == 'GET':
            test_id = request.GET.get('test_id')
            test = Test.get_by_id(int(test_id))
            candidate = User.get_by_id(test.candidate_id)
            organization = Organization.get_by_id(test.org_id)
            user = User.get_by_email(organization.contact_email)
            try:
                url = CONFIG['BASE_URL'] + "/test/start?token=" + str(test.token)
                subject = 'You are invited by ' + organization.display_name
                message = ''
                recipient_list = [candidate.email]
                fail_silently = False
                html_message = render_to_string('pages/email/email-candidate-add-test.html',
                                                {'url': url, 'name': candidate.first_name,
                                                 'time': test.expire_at.strftime('%d, %b %Y %I:%M %p'),
                                                 'organisation_name': organization.display_name,
                                                 'designation': test.designation,
                                                 'user': User._full_name(user)})
                send_email.delay(subject, message, FROM_EMAIL, recipient_list,
                                 fail_silently = fail_silently, html_message = html_message)
                messages.add_message(request, messages.SUCCESS, 'E-mail sent successfully')
            except Exception as error:
                messages.add_message(request, messages.ERROR, 'Could not send E-mail. Please try again later.')

        return HttpResponseRedirect('/test/list')

    @classmethod
    def clone(cls, request: HttpRequest):
        if request.method == 'GET':
            gender = list(Gender)
            test_id = request.GET.get('test_id')
            return render(request, '/pages/test/clone.html', {'gender': gender, 'test_id': test_id})
        elif request.method == 'POST':
            test_id = request.POST.get('test_id')
            test = Test.get_by_id(test_id)
            created_by = request.session['user_id']
            org_id = request.session['user_org_id']
            first_name = request.POST.get('first_name')
            last_name = request.POST.get('last_name')
            email = request.POST.get('email')
            mobile_number = request.POST.get('mobile_number')
            gender = int(request.POST.get('gender'))
            expire_at = request.POST.get('test_expire')
            expire_at = datetime.strptime(expire_at, '%Y-%m-%d %H:%M')
            if expire_at < datetime.now():
                messages.error(request, 'Can\'t update test. Expiry time can not be before current time.')
                return HttpResponseRedirect('/test/list')
            user = User.get_by_email(email)
            if user is None:
                user_dict = {
                    'first_name': first_name,
                    'last_name': last_name,
                    'email': email,
                    'mobile_number': mobile_number,
                    'gender': gender,
                    'created_by': created_by,
                    'password': None
                }
                user, password = User.add(user_dict)
                user_role_dict = {
                    'role_id': RoleType.CANDIDATE,
                    'user_id': user.id,
                    'org_id': org_id
                }
                user_role = UserRole.add(user_role_dict)
            test_dict = {
                'candidate_id': user.id,
                'total_questions': test.total_questions,
                'org_id': org_id,
                'skill': test.skill,
                'question_ids': test.question_ids,
                'total_time': test.total_time,
                'time_left': test.total_time,
                'status': Status.NEW,
                'created_by': created_by,
                'updated_at': None,
                'expire_at': expire_at,
                'designation': test.designation
            }
            cloned_test = Test.add(test_dict)
            test_token = TestView.generate_test_token(cloned_test.candidate_id, cloned_test.id, cloned_test.expire_at)
            cloned_test.add_token(test_token)
            TestView.send_email_to_candidate(request, expire_at, cloned_test)
            return HttpResponseRedirect('/test/list')

        return HttpResponseRedirect('/test/list')

    @classmethod
    def search(cls, request: HttpRequest):
        org_id = request.session.get('user_org_id')
        page = {'current_page': None,
                'next': None,
                'prev': None,
                'last_page': None,
                'stages': 2,
                'is_paginated': False
                }
        records_per_page = 10
        try:
            page_number = request.GET.get('page')
            page_number = int(page_number)
            if page_number < 1:
                page_number = 1
            page_number -= 1
        except Exception:
            page_number = 0
        try:
            page_number = request.GET.get('page')
            page_number = int(page_number)
            if page_number < 1:
                page_number = 1
            page_number -= 1
        except Exception:
            page_number = 0

        total = Test.get_total(org_id)
        total_pages = math.ceil((total / records_per_page))
        if request.is_ajax():
            query = request.POST.get('query', None)

            if query is None or query == '':
                # get all tests here
                if page_number >= total_pages:
                    page_number = 0
                tests = Test.search(page_number, records_per_page, org_id)
                page['is_paginated'] = True if total_pages > 1 else False
                pass

            else:
                # get search query tests by designation, status, candidate name, email, skill
                query = query.lower()
                query = query.strip()
                if query.find('new') != -1:
                    query += ' 1'
                if query.find('in_progress') != -1:
                    query += ' 2'
                if query.find('in progress') != -1:
                    query += ' 2'
                if query.find('inprogress') != -1:
                    query += ' 2'
                if query.find('closed') != -1:
                    query += ' 3'
                if query.find('expired') != -1:
                    query += ' 4'
                query = query.replace(" ", ' | ')
                tests = Test._get_search_results(query, org_id)

                all_tests = Test.get_by_org(org_id)
                candidate_ids = [test.candidate_id for test in all_tests]
                candidate_ids = list(set(candidate_ids))
                users_test = User._get_search_results(query, candidate_ids)
                tests_from_user = []
                for user in users_test:
                    res = Test.get_by_candidate_id_org_id(user.id, org_id)
                    tests_from_user.extend(res)

                skills = Skill._get_search_results(query)
                skill_tests = []
                skills = [skill.id for skill in skills]
                for test in all_tests:
                    for skill in test.skill:
                        if int(skill['skill']) in skills:
                            if test not in skill_tests:
                                skill_tests.append(test)

                tests = cls.get_unique_tests(tests, tests_from_user, skill_tests)

                page['is_paginated'] = False

            page['current_page'] = page_number + 1
            page['next'] = page_number + 2
            page['prev'] = page_number
            page['last_page'] = total_pages

            for test in tests:
                try:
                    if test.expire_at < datetime.now():
                        test.update_status(Status.EXPIRED)
                except Exception:
                    pass
            skill_ids = []
            user_list = []
            user_name = []
            user_email = []
            i = 0
            for test in tests:
                user_list.append(User.get_by_id(test.candidate_id))
                try:
                    user_name.append(user_list[i].first_name)
                    user_email.append(user_list[i].email)
                except Exception:
                    pass
                i += 1
                test_skills = test.skill
                for _test_skill in test_skills:
                    skill_ids.append(int(_test_skill['skill']))

            skill_data = Skill.get_by_ids(skill_ids)

            skill_dict = {}
            for id, name in skill_data:
                skill_dict.setdefault(id, []).append(name)

            flag = False
            if len(tests):
                flag = True

            tests = render_to_string('pages/test/table.html',
                                     {'test_list': tests, 'skill_dict': skill_dict,
                                      'base_url': CONFIG['BASE_URL'], 'user_name': user_name,
                                      'user_email': user_email, 'page': page})
            page = render_to_string('pages/pagination.html', {'page': page})

            if flag:
                data = {
                    'tests': tests,
                    'page': page
                }
            else:
                data = {
                    'questions': None,
                    'page': None
                }
            return JsonResponse(data)

    @classmethod
    def get_unique_tests(cls, test_T, test_U, test_S):
        tests = []
        for test in test_T:
            if test not in tests:
                tests.append(test)
        for test in test_U:
            if test not in tests:
                tests.append(test)
        for test in test_S:
            if test not in tests:
                tests.append(test)

        return tests

    @classmethod
    def feedback(cls, request: HttpRequest):
        if request.is_ajax():
            test_id = request.POST.get('test_id')
            feedback = request.POST.get('feedback')
            test = Test.get_by_id(test_id)
            try:
                test.update_candidate_feedback(feedback)
                response = {
                    'success': True
                }
            except Exception as e:
                response = {
                    'success': False
                }
            return JsonResponse(response)


