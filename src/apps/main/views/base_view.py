from django.contrib import messages
from django.http import HttpRequest, Http404, HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.utils.encoding import escape_uri_path
from django.views import View


def is_super_admin(func):
    # DECORATOR
    def wrapper(base_view_object, request, *args, **kwargs):
        if 'is_super_admin' in request.session:
            return func(base_view_object, request)
        else:
            return render(request, 'layouts/404.html', {})

    return wrapper


def is_org_admin(func):
    def wrapper(base_view_object, request, *args, **kwargs):
        if 'is_org_admin' in request.session:
            return func(base_view_object, request)
        else:
            messages.error(request, "Please login to access.")
            return HttpResponseNotFound("Oops. How did you land here?")

    return wrapper


def is_on_chrome_desktop(func):
    # DECORATOR
    def wrapper(base_view_object, request, *args, **kwargs):
        if request.user_agent.is_pc and (request.user_agent.browser.family == 'Chrome' or request.user_agent.browser.family == 'Chromium'):
            return func(base_view_object, request)
        else:
            return HttpResponseRedirect('/test/use_chrome')
    return wrapper


class BaseView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def dispatch(self, request: HttpRequest, *args, **kwargs):
        uri = escape_uri_path(request.path)
        segments = uri.lstrip('/').rstrip('/').split('/')
        segment_count = len(segments)
        handler = None
        if segments[0] == 'app':
            if segment_count == 3:
                handler = getattr(self, segments[2], None)
                if handler is None:
                    handler = getattr(self, 'index', None)
            elif segment_count >= 4:
                handler = getattr(self, segments[3], None)
        else:
            if segments[segment_count - 1] == '':
                handler = getattr(self, 'index', None)
            else:
                handler = getattr(self, segments[segment_count - 1], None)
        if handler is not None:
            try:
                return handler(request)
            except Exception as e:
                raise e
        else:
            raise Http404()
