import math
from django.http import HttpRequest, JsonResponse
from django.template.loader import render_to_string
from django.template.response import TemplateResponse

from apps.main.models.question import Question
from apps.main.models.result import Result
from apps.main.models.test import Test
from apps.main.models.user import User
from apps.main.views.base_view import BaseView


class ResultView(BaseView):

    @staticmethod
    def list(request):
        page = {'current_page': None,
                'next': None,
                'prev': None,
                'last_page': None,
                'stages': 2,
                'is_paginated': False
                }
        records_per_page = 10
        users = User.search()
        candidate_list = {}
        for user in users:
            candidate_list[user.id] = user.first_name + " " + user.last_name
        try:
            page_number = request.GET.get('page')
            page_number = int(page_number)
            if page_number < 1:
                page_number = 1
            page_number -= 1
        except Exception:
            page_number = 0

        organisation = int(request.session.get('user_org_id'))
        total = Result.get_total(organisation)
        total_pages = math.ceil((total / records_per_page))
        if page_number >= total_pages:
            page_number = 0
        result_list = Result.search(page_number, records_per_page, organisation)
        page['is_paginated'] = True if total_pages > 1 else False
        page['current_page'] = page_number + 1
        page['next'] = page_number + 2
        page['prev'] = page_number
        page['last_page'] = total_pages

        return TemplateResponse(request, 'pages/result/list.html',
                                {'results': result_list,
                                 'candidates': candidate_list,
                                 'page': page})

    @staticmethod
    def view_answer(request: HttpRequest):
        result_id = request.POST.get('result_id')
        answers = Result.get_answer(result_id)
        user = Result.get_candidate(result_id)
        result = Result.get_by_id(result_id)
        test_id = result.test_id
        test = Test.get_by_id(test_id)

        questions = Question.list_all()
        questions_list = {}
        answers_list = {}
        for question in questions:
            questions_list[question.id] = question.title
            answers_list[question.id] = question.answer_options

        try:
            name = user.first_name + " " + user.last_name
        except Exception:
            name = "User is Deleted"
        data = {
            'answers': answers.answers,
            'name': name,
            'questions_list': questions_list,
            'answers_list': answers_list,
            'test': test
        }

        return TemplateResponse(request, 'pages/result/view_answer.html', data)

    @classmethod
    def search(cls, request: HttpRequest):
        org_id = int(request.session.get('user_org_id'))
        page = {'current_page': None,
                'next': None,
                'prev': None,
                'last_page': None,
                'stages': 2,
                'is_paginated': False
                }
        try:
            page_number = request.GET.get('page')
            page_number = int(page_number)
            if page_number < 1:
                page_number = 1
            page_number -= 1
        except Exception:
            page_number = 0
        if request.is_ajax():
            query = request.POST.get('query', None)

            if query is None or query == '':
                records_per_page = 10
                users = User.search()
                candidate_list = {}
                for user in users:
                    candidate_list[user.id] = user.first_name + " " + user.last_name
                total = Result.get_total(org_id)
                total_pages = math.ceil((total / records_per_page))

                if page_number >= total_pages:
                    page_number = 0
                result_list = Result.search(page_number, records_per_page, org_id)
                page['is_paginated'] = True if total_pages > 1 else False
                page['current_page'] = page_number + 1
                page['next'] = page_number + 2
                page['prev'] = page_number
                page['last_page'] = total_pages

            else:
                query = query.strip()
                query = query.replace(" ", ' | ')
                users = User._get_search_results(query)
                user_ids = [user.id for user in users]
                user_ids = tuple(user_ids)
                result_list = Result._get_by_candidate_ids_and_org_id(user_ids, org_id)
                candidate_list = {}
                for user in users:
                    candidate_list[user.id] = user.first_name + " " + user.last_name

                page['is_paginated'] = False

            flag = False
            if len(result_list):
                flag = True

            results = render_to_string('pages/result/table.html',
                                       {'results': result_list,
                                        'candidates': candidate_list,
                                        'page': page})
            page = render_to_string('pages/pagination.html', {'page': page})
            if flag:
                data = {
                    'results': results,
                    'page': page
                }
            else:
                data = {
                    'results': None,
                    'page': None
                }
            return JsonResponse(data)

    @classmethod
    def view_feedback(cls, request: HttpRequest):
        test_id = request.POST.get('test_id')
        test = Test.get_by_id(test_id)
        feedback = test.candidate_feedback
        try:
            if len(feedback) == 0 or feedback == None:
                feedback = 'No feedback was given by the candidate.'
        except Exception as e:
            feedback = 'No feedback was given by the candidate.'

        return TemplateResponse(request, 'pages/result/view_feedback.html',
                                {'feedback': feedback})
