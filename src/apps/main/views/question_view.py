import os
import math
from datetime import datetime
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpRequest, JsonResponse
from django.shortcuts import render
from django.template.response import TemplateResponse
from django.template.loader import render_to_string

from apps.main.models.base import session
from apps.main.models.question import Question, QuestionType
from apps.main.models.question_difficulty import DifficultyLevel, QuestionDifficulty
from apps.main.models.question_skill import QuestionSkill
from apps.main.models.skill import Skill
from apps.main.views.base_view import BaseView, is_super_admin


class QuestionView(BaseView):
    @is_super_admin
    def add(self, request):
        if request.method == 'GET':
            skills = session.query(Skill).all()
            difficulty_level_list = list(DifficultyLevel)
            return render(request, 'pages/question/add.html',
                          {'skills': skills, 'difficulty_level_list': difficulty_level_list})
        elif request.method == 'POST':
            question = request.POST.get('question')
            question_description = request.POST.get('description')
            question_type = request.POST.get('question_type')
            skill_ids_str = request.POST.getlist('skills')
            skill_ids = list(map(int, skill_ids_str))
            file_name = QuestionView.save_question_description_image(request)

            # experience_difficulty_levels[0] is for 1+ years experience
            experience_difficulty_levels = []
            for i in range(1, 7):
                exp_difficulty = request.POST.get('experience' + str(i))
                experience_difficulty_levels.append(exp_difficulty)

            if question_type == 'descriptive':
                type = QuestionType.DESCRIPTIVE
                answer_options = request.POST.get('answer_description')

            elif question_type == 'multiple_correct' or question_type == 'single_correct':
                answer_options = {}
                if question_type == 'multiple_correct':
                    type = QuestionType.MULTIPLE_CORRECT
                else:
                    type = QuestionType.SINGLE_CORRECT

                answer_fields = request.POST.getlist('answer_fields')
                correct_answer_indices = request.POST.getlist('checks')
                correct_answer_indices_int = list(map(int, correct_answer_indices))
                # answer_options = {'Python': True, 'Java': False, 'JS': True}
                for index, val in enumerate(answer_fields):
                    if index in correct_answer_indices_int:
                        answer_options.update({val: True})
                    else:
                        answer_options.update({val: False})

            data = {'title': question, 'answer_options': answer_options, 'type': type,
                    'description': question_description, 'created_by': request.session['user_id'],
                    'description_image': file_name}

            question = Question.add(data)
            if file_name:
                QuestionView.replace_file(question.id, file_name)
            for index, exp_difficulty in enumerate(experience_difficulty_levels):
                if exp_difficulty:
                    experience_level = index + 1
                    difficulty_level = int(exp_difficulty)
                    question_difficulty_dict = {'question_id': question.id,
                                                'difficulty_level': difficulty_level,
                                                'experience_level': experience_level}

                    QuestionDifficulty.add(question_difficulty_dict)

            for s_id in skill_ids:
                question_skill_dict = {'skill_id': s_id, 'question_id': question.id}
                QuestionSkill.add(question_skill_dict)

            messages.add_message(request, messages.SUCCESS, 'Question added successfully.')
            return HttpResponseRedirect('/question/list')

    @is_super_admin
    def list(self, request):
        page = {'current_page': None,
                'next': None,
                'prev': None,
                'last_page': None,
                'stages': 2,
                'is_paginated': False
                }
        records_per_page = 10
        try:
            page_number = request.GET.get('page')
            page_number = int(page_number)
            if page_number < 1:
                page_number = 1
            page_number -= 1
        except Exception:
            page_number = 0

        total = Question.get_total()
        total_pages = math.ceil((total / records_per_page))
        if page_number >= total_pages:
            page_number = 0
        question_list = Question.search(page_number, records_per_page)
        page['is_paginated'] = True if total_pages > 1 else False
        page['current_page'] = page_number + 1
        page['next'] = page_number + 2
        page['prev'] = page_number
        page['last_page'] = total_pages
        skill = []
        for i in range(len(question_list)):
            qs_id = QuestionSkill.get_by_question_id(question_list[i].id)
            skill.append(Skill.get_by_id(qs_id.skill_id).title)
        return TemplateResponse(request, 'pages/question/list.html', {'questions': question_list, 'skill': skill,
                                                                      'page': page})

    @is_super_admin
    def deactivate(self, request):
        question_id = request.GET['question_id']
        if request.GET.get('page'):
            page_number = request.GET.get('page')
        else:
            page_number = '1'
        question = Question.get_by_id(question_id)
        Question.deactivate(question)
        skill = QuestionSkill.get_by_question_id(question.id)
        question_skill = QuestionSkill.get_active_questions_by_skill(skill.skill_id)
        skill = Skill.get_by_id(skill.skill_id)
        if not question_skill:
            skill.update_deactivated_at(datetime.now().timestamp())
        messages.add_message(request, messages.SUCCESS, 'Question deactivated successfully.')
        return HttpResponseRedirect('/question/list?page=' + str(page_number))

    @is_super_admin
    def activate(self, request):
        question_id = request.GET['question_id']
        if request.GET.get('page'):
            page_number = request.GET.get('page')
        else:
            page_number = '1'
        question = Question.get_by_id(question_id)
        Question.activate(question)
        skill = QuestionSkill.get_by_question_id(question.id)
        question_skill = QuestionSkill.get_active_questions_by_skill(skill.skill_id)
        skill = Skill.get_by_id(skill.skill_id)
        if question_skill:
            skill.update_deactivated_at(None)
        messages.add_message(request, messages.SUCCESS, 'Question activated successfully.')
        return HttpResponseRedirect('/question/list?page=' + str(page_number))

    @is_super_admin
    def view(self, request):
        question_id = request.GET['question_id']
        question = Question.get_by_id(question_id)
        return TemplateResponse(request, 'pages/question/view.html', {'question': question})

    @is_super_admin
    def update(self, request: HttpRequest):
        q_id = request.GET.get('question_id')
        if request.GET.get('page'):
            page_number = request.GET.get('page')
        else:
            page_number = '1'
        question = Question.get_by_id(int(q_id))
        if request.method == 'GET':
            description_image = question.description_image
            if description_image:
                index = description_image.find('_')
                description_image = description_image[index + 1:]
            return render(request, 'pages/question/update.html', {'question': question,
                                                                  'description_image': description_image,
                                                                  'page': page_number})
        elif request.method == 'POST':
            is_image_available = request.POST.get('description_image_tracker')
            if is_image_available != '':
                file_name = QuestionView.save_question_description_image(request)
                question.update_description_image(file_name)
                QuestionView.replace_file(question.id, file_name)

            title = request.POST.get('question')
            description = request.POST.get('description')
            question.update_title(title)
            question.update_description(description)
            messages.success(request, 'Question was edited successfully.')

        return HttpResponseRedirect('/question/list?page=' + str(page_number))

    @classmethod
    def save_question_description_image(cls, request):
        if request.FILES:
            if os.path.exists("media"):
                if os.path.exists("media/question"):
                    if os.path.exists("media/question/dump"):
                        pass
                    else:
                        os.mkdir('media/question/dump')
                else:
                    os.mkdir('media/question')
                    os.mkdir('media/question/dump')
            else:
                os.mkdir('media')
                os.mkdir('media/question')
                os.mkdir('media/question/dump')

            uploaded_image = request.FILES['file_upload']
            file_name = uploaded_image
        else:
            file_name = None

        if file_name:
            file_name = str(datetime.now().timestamp()).replace('.', '') + '_' + str(file_name)
            with open('media/question/dump/' + file_name, 'wb+') as file:
                for chunk in uploaded_image.chunks():
                    file.write(chunk)
        return file_name

    @classmethod
    def replace_file(cls, question_id, file_name):
        if file_name:
            if os.path.exists('media/question/' + str(question_id)):
                pass
            else:
                os.mkdir('media/question/' + str(question_id))
            os.rename('media/question/dump/' + file_name, 'media/question/' + str(question_id) + '/' + file_name)

    @classmethod
    def search(cls, request: HttpRequest):
        page = {'current_page': None,
                'next': None,
                'prev': None,
                'last_page': None,
                'stages': 2,
                'is_paginated': False
                }
        try:
            page_number = request.GET.get('page')
            page_number = int(page_number)
            if page_number < 1:
                page_number = 1
            page_number -= 1
        except Exception:
            page_number = 0
        if request.is_ajax():
            query = request.POST.get('query', None)

            if query is None or query == '':
                records_per_page = 10
                total = Question.get_total()
                total_pages = math.ceil((total / records_per_page))

                if page_number >= total_pages:
                    page_number = 0
                questions = Question.search(page_number, records_per_page)
                page['is_paginated'] = True if total_pages > 1 else False
                page['current_page'] = page_number + 1
                page['next'] = page_number + 2
                page['prev'] = page_number
                page['last_page'] = total_pages

            else:
                query = query.strip()
                query = query.replace(" ", ' | ')
                questions = Question._get_search_results(query)
                skills = Skill._get_search_results(query)
                q_skill = []
                for skill in skills:
                    res = QuestionSkill.get_all_questions_by_skill(skill.id)
                    q_skill.extend(res)
                questions = cls.get_unique_questions(questions, q_skill)
                page['is_paginated'] = False

            skill = []
            for i in range(len(questions)):
                qs_id = QuestionSkill.get_by_question_id(questions[i].id)
                skill.append(Skill.get_by_id(qs_id.skill_id).title)

            flag = False
            if len(questions):
                flag = True

            questions = render_to_string('pages/question/table.html',
                                         {'questions': questions, 'skill': skill, 'page': page})
            page = render_to_string('pages/pagination.html', {'page': page})
            if flag:
                data = {
                    'questions': questions,
                    'page': page
                }
            else:
                data = {
                    'questions': None,
                    'page': None
                }
            return JsonResponse(data)

    @classmethod
    def get_unique_questions(cls, question_A, question_B):

        # question_A and question_B are two lists with questions and
        # this method will return a single list with the union
        # of both the list

        for qs in question_B:
            if qs not in question_A:
                question_A.append(qs)

        return question_A
