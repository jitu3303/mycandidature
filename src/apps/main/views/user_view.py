from django.contrib import messages
from django.http import HttpResponseRedirect, HttpRequest
from django.shortcuts import render, redirect

from apps.main.models.user import User
from apps.main.models.organization import Organization
from apps.main.models.user_role import UserRole, RoleType
from apps.main.views.base_view import BaseView


class UserView(BaseView):

    @staticmethod
    def login(request: HttpRequest):
        return render(request, 'pages/user/login.html', {})

    @staticmethod
    def authenticate(request: HttpRequest):
        if request.method == 'POST':
            email = request.POST['email']
            password = request.POST['password']
            user = User.authenticate(email, password)
            if user:
                user_role = UserRole.get_by_user_id(user.id)
                request.session['user_role_id'] = user_role.role_id.value
                request.session['user_org_id'] = user_role.org_id
                if user_role.role_id.value == RoleType.SUPER_ADMIN.value:
                    request.session['is_super_admin'] = True
                elif user_role.role_id.value == RoleType.ORG_ADMIN.value:
                    if Organization.get_by_mail(email).is_active == False:
                        messages.error(request, 'Organization is deactivated, please contact the administrator')
                        return HttpResponseRedirect('/user/login')
                    else:
                        request.session['is_org_admin'] = True
                else:
                    messages.error(request, 'Invalid username or password.')
                    return HttpResponseRedirect('/user/login')

                request.session['user_id'] = user.id
                request.session['user_first_name'] = user.first_name
                request.session['user_last_name'] = user.last_name
                request.session['user_email'] = user.email
                try:
                    if request.session['is_super_admin'] == True:
                        return HttpResponseRedirect('/skill/list')
                except Exception:
                    return HttpResponseRedirect('/test/list')

        messages.error(request, 'Invalid username or password.')
        return HttpResponseRedirect('/user/login')

    # organization = Organization.get_by_mail(email)
    # if organization.is_active == True:
    #     request.session['is_org_admin'] = True
    # else:
    #     messages.error(request, 'Organization is deactive')
    #     return HttpResponseRedirect('/user/login')
    @staticmethod
    def logout(request: HttpRequest):
        try:
            del request.session['user_id']
            del request.session['user_first_name']
            del request.session['user_last_name']
            del request.session['user_email']
            if 'is_super_admin' in request.session:
                del request.session['is_super_admin']
            if 'is_org_admin' in request.session:
                del request.session['is_org_admin']
        except:
            pass

        return HttpResponseRedirect('/user/login')

    @staticmethod
    def change_password(request: HttpRequest):
        if request.method == 'POST':
            email = request.session['user_email']
            old_password = request.POST['old_password']
            new_password = request.POST['new_password']
            re_entered_password = request.POST['re_entered_password']
            user = User.authenticate(email, old_password)

            if not user:
                messages.add_message(request, messages.ERROR, "You have entered a wrong current password.")
                return redirect('/user/change_password')

            if str(new_password) != str(re_entered_password):
                # redirect with an alert message
                text_message = 'You have Re-Entered a different password.'
                messages.add_message(request, messages.ERROR, text_message)

                return redirect('/user/change_password', )

            if str(old_password) == str(new_password):
                messages.add_message(request, messages.ERROR, "New Password can not be the same as Current Password")
                return redirect('/user/change_password')

            User.update_password(user, new_password)
            messages.add_message(request, messages.SUCCESS, 'Success! Your Password has been changed.')
            try:
                if request.session['is_super_admin'] == True:
                    return HttpResponseRedirect('/skill/list')
            except Exception:
                return HttpResponseRedirect('/test/list')
        else:
            return render(request, 'pages/user/change_password.html')

