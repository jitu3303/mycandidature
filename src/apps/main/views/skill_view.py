from datetime import datetime
from django.contrib import messages
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import render
from django.template.defaultfilters import slugify
from django.template.response import TemplateResponse
from psycopg2._psycopg import IntegrityError
from sqlalchemy.exc import IntegrityError

from apps.main.models.skill import Skill
from apps.main.models.question import Question
from apps.main.models.question_skill import QuestionSkill
from apps.main.views.base_view import BaseView, is_super_admin


class SkillView(BaseView):
    @is_super_admin
    def add(self, request: HttpRequest):
        if request.method == 'GET':
            return TemplateResponse(request, 'pages/skill/add.html')
        elif request.method == 'POST':
            request_dict = {x: request.POST.get(x) for x in request.POST.keys()}
            title = request.POST.get('title')
            slug = slugify(title)
            special_characters = ['+', '#']
            if any(c in title for c in special_characters):
                data = {'title': title.upper(), 'slug': title.lower()}
            else:
                data = {'title': title.upper(), 'slug': slug}
            # data = {'title': title.upper(), 'slug': slug}
            try:
                Skill.add(data)
            except AssertionError as error:
                messages.add_message(request, messages.ERROR, error)
                return render(request, 'pages/skill/add.html', request_dict)
            except Exception as error:
                if isinstance(error, IntegrityError):
                    messages.add_message(request, messages.ERROR, "Skill already exists.")
                else:
                    messages.add_message(request, messages.ERROR, "Something went wrong.")
                return render(request, 'pages/skill/add.html', request_dict)

            messages.add_message(request, messages.SUCCESS, title + ' added successfully.')
            return HttpResponseRedirect('/skill/list')

    @is_super_admin
    def list(self, request: HttpRequest):
        skill_objects = Skill.list_all()
        # for first time running after migration
        for i in range(len(skill_objects)):
            question = QuestionSkill.get_active_questions_by_skill(skill_objects[i].id)
            if not question:
                skill_objects[i].deactivated_at = datetime.now().timestamp()
        return TemplateResponse(request, 'pages/skill/list.html', {'skills': skill_objects})

    @is_super_admin
    def deactivate(self, request: HttpRequest):
        skill_id = request.GET.get('skill_id')
        skill = Skill.get_by_id(int(skill_id))
        skill.update_deactivated_at(datetime.now().timestamp())
        questions = QuestionSkill.get_active_questions_by_skill(int(skill_id))
        for question in questions:
            Question.deactivate(question)

        messages.error(request, '{0} skill is deactivated.'.format(skill.title))
        return HttpResponseRedirect('/skill/list')

    @is_super_admin
    def activate(self, request: HttpRequest):
        skill_id = request.GET.get('skill_id')
        skill = Skill.get_by_id(int(skill_id))
        questions = QuestionSkill.get_all_questions_by_skill(int(skill_id))
        if questions:
            skill.update_deactivated_at(None)
            for question in questions:
                Question.activate(question)

            messages.success(request, '{0} skill is activated.'.format(skill.title))
        else:
            messages.error(request, '{0} skill can not be activated, it has no questions.'.format(skill.title))
        return HttpResponseRedirect('/skill/list')
