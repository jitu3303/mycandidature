"""add is active in organization

Revision ID: 0017
Revises: 0016
Create Date: 2019-07-27 17:50:48.018189

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0017'
down_revision = '0016'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('organization', sa.Column('is_active', sa.Boolean(), nullable=False, default=True))

def downgrade():
    op.drop_column('organization', 'is_active')
