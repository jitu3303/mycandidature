"""Add status in test

Revision ID: 0013
Revises: 0012
Create Date: 2019-06-06 12:49:56.411376

"""
import enum

import sqlalchemy as sa
import sqlalchemy_utils
from alembic import op

# revision identifiers, used by Alembic.
revision = '0013'
down_revision = '0012'
branch_labels = None
depends_on = None


class Status(enum.Enum):
    NEW = 1
    IN_PROGRESS = 2
    CLOSED = 3


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('test', sa.Column('status', sqlalchemy_utils.types.choice.ChoiceType(Status, impl=sa.Integer()),
                                    nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('test', 'status')
    # ### end Alembic commands ###
