"""changed time stamp of results from datetime now to utcnow

Revision ID: 0019
Revises: 0018
Create Date: 2020-03-24 18:10:50.889697

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0019'
down_revision = '0018'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
