"""string length increased in name of user

Revision ID: 0018
Revises: 0017
Create Date: 2019-08-02 14:28:29.874221

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0018'
down_revision = '0017'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('users', 'first_name',
                    existing_type=sa.String(length=20),
                    type_=sa.String(length=40),
                    existing_nullable=False)
    op.alter_column('users', 'last_name',
                    existing_type=sa.String(length=20),
                    type_=sa.String(length=40),
                    existing_nullable=False)


def downgrade():
    op.alter_column('users', 'first_name',
                    existing_type=sa.String(length=40),
                    type_=sa.String(length=20),
                    existing_nullable=False)
    op.alter_column('users', 'last_name',
                    existing_type=sa.String(length=40),
                    type_=sa.String(length=20),
                    existing_nullable=False)
