"""Add total time to test table and make nullable false

Revision ID: 0009
Revises: 0008
Create Date: 2019-06-03 17:16:46.509590

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '0009'
down_revision = '0008'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('organization', 'city',
                    existing_type=sa.VARCHAR(length=20),
                    nullable=True)
    op.alter_column('organization', 'contact_address',
                    existing_type=sa.VARCHAR(length=150),
                    nullable=True)
    op.alter_column('organization', 'contact_email',
                    existing_type=sa.VARCHAR(length=50),
                    nullable=True)
    op.alter_column('organization', 'display_name',
                    existing_type=sa.VARCHAR(length=50),
                    nullable=True)
    op.alter_column('question', 'title',
                    existing_type=sa.VARCHAR(length=400),
                    nullable=True)
    op.alter_column('test', 'candidate_id',
                    existing_type=sa.INTEGER(),
                    nullable=True)
    op.alter_column('test', 'created_at',
                    existing_type=postgresql.TIMESTAMP(),
                    nullable=True)
    op.alter_column('test', 'org_id',
                    existing_type=sa.INTEGER(),
                    nullable=True)
    op.alter_column('test', 'total_questions',
                    existing_type=sa.INTEGER(),
                    nullable=True)
    op.alter_column('test', 'updated_at',
                    existing_type=postgresql.TIMESTAMP(),
                    nullable=True)
    op.alter_column('user_role', 'org_id',
                    existing_type=sa.INTEGER(),
                    nullable=True)
    op.alter_column('user_role', 'user_id',
                    existing_type=sa.INTEGER(),
                    nullable=True)
    op.alter_column('users', 'email',
                    existing_type=sa.VARCHAR(length=50),
                    nullable=True)
    op.alter_column('users', 'gender',
                    existing_type=sa.INTEGER(),
                    nullable=True)
    op.alter_column('users', 'password',
                    existing_type=sa.VARCHAR(length=100),
                    nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('users', 'password',
                    existing_type=sa.VARCHAR(length=100),
                    nullable=False)
    op.alter_column('users', 'gender',
                    existing_type=sa.INTEGER(),
                    nullable=False)
    op.alter_column('users', 'email',
                    existing_type=sa.VARCHAR(length=50),
                    nullable=False)
    op.alter_column('user_role', 'user_id',
                    existing_type=sa.INTEGER(),
                    nullable=False)
    op.alter_column('user_role', 'org_id',
                    existing_type=sa.INTEGER(),
                    nullable=False)
    op.alter_column('test', 'updated_at',
                    existing_type=postgresql.TIMESTAMP(),
                    nullable=False)
    op.alter_column('test', 'total_questions',
                    existing_type=sa.INTEGER(),
                    nullable=False)
    op.alter_column('test', 'org_id',
                    existing_type=sa.INTEGER(),
                    nullable=False)
    op.alter_column('test', 'created_at',
                    existing_type=postgresql.TIMESTAMP(),
                    nullable=False)
    op.alter_column('test', 'candidate_id',
                    existing_type=sa.INTEGER(),
                    nullable=False)
    op.drop_column('test', 'total_time')
    op.alter_column('question', 'title',
                    existing_type=sa.VARCHAR(length=400),
                    nullable=False)
    op.alter_column('organization', 'display_name',
                    existing_type=sa.VARCHAR(length=50),
                    nullable=False)
    op.alter_column('organization', 'contact_email',
                    existing_type=sa.VARCHAR(length=50),
                    nullable=False)
    op.alter_column('organization', 'contact_address',
                    existing_type=sa.VARCHAR(length=150),
                    nullable=False)
    op.alter_column('organization', 'city',
                    existing_type=sa.VARCHAR(length=20),
                    nullable=False)
    # ### end Alembic commands ###
