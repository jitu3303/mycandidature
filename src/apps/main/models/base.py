from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy_mixins import ActiveRecordMixin, ReprMixin

from my_candidature import settings

Base = declarative_base()

# SQLALCHEMY_URL = "postgresql+psycopg2://archita:archita@localhost/mycandidature"

DATABASE_NAME = settings.DATABASES['default']['NAME']
DB_USERNAME = settings.DATABASES['default']['USER']
DB_PASSWORD = settings.DATABASES['default']['PASSWORD']
DB_HOST = settings.DATABASES['default']['HOST']

SQLALCHEMY_URL = "postgresql+psycopg2://" + DB_USERNAME + ":" + DB_PASSWORD + '@' + DB_HOST + '/' + DATABASE_NAME

engine = create_engine(SQLALCHEMY_URL)
session = scoped_session(sessionmaker(bind=engine, autocommit=True))

Base.metadata.create_all(engine)


def log(msg):
    print('\n{}\n'.format(msg))


# Used SQLAlchemy Mixin
# we also use ReprMixin which is optional
class BaseModel(Base, ActiveRecordMixin, ReprMixin):
    __abstract__ = True
    __repr__ = ReprMixin.__repr__
    pass


BaseModel.set_session(session)
