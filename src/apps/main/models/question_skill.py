from sqlalchemy import Column, Integer, UniqueConstraint

from apps.main.models.base import BaseModel, session
from apps.main.models.question import Question


class QuestionSkill(BaseModel):
    __tablename__ = 'question_skill'

    id = Column(Integer, primary_key=True, autoincrement=True)
    skill_id = Column(Integer)
    question_id = Column(Integer)

    __table_args__ = (
        UniqueConstraint('skill_id', 'question_id', name='_question_skill_uk'),
    )

    @classmethod
    def add(cls, data):
        question_skill = QuestionSkill()
        question_skill.fill(**data)
        question_skill.save()
        return question_skill

    @classmethod
    def get_by_question_id(cls, question_id):
        return session.query(QuestionSkill).filter(cls.question_id == question_id).first()

    @staticmethod
    def list_all():
        return session.query(QuestionSkill).order_by(QuestionSkill.id.desc()).all()

    # Returns questions randomly for a skill
    @classmethod
    def get_active_questions_by_skill(cls, skill_id):
        questions = session.query(Question) \
            .filter(Question.is_active == True) \
            .filter(Question.id == QuestionSkill.question_id) \
            .filter(QuestionSkill.skill_id == skill_id).all()
        return questions


    @classmethod
    def get_all_questions_by_skill(cls, skill_id):
        questions = session.query(Question) \
            .filter(Question.id == QuestionSkill.question_id) \
            .filter(QuestionSkill.skill_id == skill_id).all()
        return questions
