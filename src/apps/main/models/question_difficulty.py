import enum

from sqlalchemy import Column, Integer, UniqueConstraint
from sqlalchemy_utils import ChoiceType

from apps.main.models.base import BaseModel, session


class DifficultyLevel(enum.Enum):
    EASY = 1
    MEDIUM = 2
    HARD = 3


class DifficultySeconds(enum.Enum):
    EASY = 180
    MEDIUM = 300
    HARD = 360


class QuestionDifficulty(BaseModel):
    __tablename__ = 'question_difficulty'

    id = Column(Integer, primary_key=True, autoincrement=True)
    question_id = Column(Integer)
    difficulty_level = Column(ChoiceType(DifficultyLevel, impl=Integer()), nullable=False)
    experience_level = Column(Integer)

    __table_args__ = (
        UniqueConstraint('question_id', 'experience_level', name='_question_experience_uk'),
    )

    @classmethod
    def add(cls, data):
        question_difficulty = QuestionDifficulty()
        question_difficulty.fill(**data)
        question_difficulty.save()
        return question_difficulty

    # Select question_id from QuestionDifficulty where experience_level is temp and question_id in question_ids
    @classmethod
    def get_qs_by_experience(cls, question_ids, experience_level):
        return session.query(QuestionDifficulty) \
            .filter(QuestionDifficulty.question_id.in_(question_ids)) \
            .filter(QuestionDifficulty.experience_level == experience_level).all()
