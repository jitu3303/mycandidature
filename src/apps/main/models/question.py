import datetime
import enum
from numbers import Number

from sqlalchemy import Column, Integer, String, JSON, Text, DateTime, Boolean, desc, func, text
from sqlalchemy_utils import ChoiceType

from apps.main.models.base import session, BaseModel


class QuestionType(enum.Enum):
    DESCRIPTIVE = 1
    MULTIPLE_CORRECT = 2
    SINGLE_CORRECT = 3


class Question(BaseModel):
    __tablename__ = 'question'
    id = Column(Integer, primary_key = True, autoincrement = True)
    title = Column(String(400))
    description = Column(Text)
    type = Column(ChoiceType(QuestionType, impl = Integer()), nullable = False)
    answer_options = Column(JSON)
    is_active = Column(Boolean, nullable = False, default = True)
    description_image = Column(String(255))
    created_at = Column(DateTime, default = datetime.datetime.now())
    created_by = Column(Integer)
    updated_at = Column(DateTime, onupdate = datetime.datetime.now())
    updated_by = Column(Integer)

    @classmethod
    def add(cls, data):
        question = Question()
        question.fill(**data)
        question.save()
        return question

    @staticmethod
    def list_all():
        return session.query(Question).order_by(Question.id.desc()).all()

    @staticmethod
    def get_by_id(question_id):
        return session.query(Question).filter_by(id = question_id).first()

    @classmethod
    def deactivate(cls, question):
        question.is_active = False
        question.save()
        return question

    @classmethod
    def activate(cls, question):
        question.is_active = True
        question.save()
        return question

    def update_title(self, title):
        self.update(title = title)

    def update_description(self, description):
        self.update(description = description)

    def update_description_image(self, description_image):
        self.update(description_image = description_image)

    @classmethod
    def get_total(cls):
        return session.query(Question).count()

    @classmethod
    def search(cls, page, record_per_page):
        search_query = session.query(Question)

        search_query = search_query.order_by(desc(Question.id))

        if isinstance(page, Number):
            search_query = search_query.limit(record_per_page).offset(
                int(page) * record_per_page)

        search_result = search_query.all()

        return search_result

    @classmethod
    def _get_search_results(cls, query):
        # select * from question where to_tsvector(question.title || ' ' || question.description) @@ to_tsquery('image');
        results_questions = None
        try:
            results_questions = session.query(Question).filter(text("to_tsvector(Question.description || ' ' || Question.title) @@ to_tsquery(:search_string)")).params(search_string=query).all()

        except Exception as e:
            print("__ERROR__ ", e)

        return results_questions

