import enum

from sqlalchemy import Column, Integer, UniqueConstraint
from sqlalchemy_utils import ChoiceType

from apps.main.models.base import BaseModel, session


class RoleType(enum.Enum):
    SUPER_ADMIN = 1
    ORG_ADMIN = 2
    CANDIDATE = 3


class UserRole(BaseModel):
    __tablename__ = 'user_role'

    id = Column(Integer, primary_key=True, autoincrement=True)
    role_id = Column(ChoiceType(RoleType, impl=Integer()), nullable=False)
    user_id = Column(Integer)
    org_id = Column(Integer)

    __table_args__ = (
        UniqueConstraint('role_id', 'user_id', 'org_id', name='_role_user_org_uk'),
    )

    @classmethod
    def add(cls, data):
        user_role = UserRole()
        user_role.fill(**data)
        user_role.save()
        return user_role

    @classmethod
    def get_by_id(cls, user_role_id):
        return session.query(cls).filter(cls.id == user_role_id).first()

    @classmethod
    def get_by_org_id(cls, org_id):
        return session.query(cls).filter(cls.org_id == org_id).first()

    @classmethod
    def get_by_user_id(cls, user_id):
        return session.query(cls).filter(cls.user_id == user_id).first()

