import datetime
from numbers import Number

from sqlalchemy import Column, Integer, JSON, DateTime, desc
from sqlalchemy.orm.attributes import flag_modified

from apps.main.models.base import session, BaseModel
from apps.main.models.user import User


class Result(BaseModel):
    __tablename__ = 'result'
    id = Column(Integer, primary_key = True, autoincrement = True)
    test_id = Column(Integer)
    candidate_id = Column(Integer)
    answers = Column(JSON)
    score = Column(Integer)
    org_id = Column(Integer)
    created_at = Column(DateTime, default = datetime.datetime.now())

    @classmethod
    def add(cls, data):
        result = Result()
        data['answers'] = [data['answers']]
        result.fill(**data)
        result.save()

    @staticmethod
    def get_by_id(result_id):
        return session.query(Result).filter(Result.id == result_id).first()

    @staticmethod
    def get_by_test(test_id):
        return session.query(Result).filter(Result.test_id == test_id).first()

    def update_answers(self, result_dict):
        all_answers_in_db = self.answers
        question_id_exists = False
        for index, answer in enumerate(all_answers_in_db):
            if answer['question_id'] == result_dict['answers']['question_id']:
                question_id_exists = True
                answer_index = index
        if question_id_exists:
            new_answers = self.answers
            new_answers[answer_index]['answers'] = result_dict['answers']['answers']
        else:
            new_answers = self.answers + [result_dict['answers']]

        # NOTE: Use flag_modified when updating JSON data
        flag_modified(self, 'answers')

        self.update(answers = new_answers)

    @staticmethod
    def search_all():
        return session.query(Result).all()

    @classmethod
    def get_answer(cls, result_id):
        return session.query(cls).filter(cls.id == result_id).first()

    @classmethod
    def get_candidate(cls, result_id):
        return session.query(User) \
            .filter(User.id == Result.candidate_id) \
            .filter(Result.id == result_id).first()

    @classmethod
    def get_answers_by_test_id(cls, test_id):
        return session.query(cls.answers) \
            .filter(cls.test_id == test_id).first()

    @staticmethod
    def search(page, record_per_page, org_id):
        search_query = session.query(Result)

        search_query = search_query.filter_by(org_id = org_id).order_by(desc(Result.id))

        if isinstance(page, Number):
            search_query = search_query.limit(record_per_page).offset(
                int(page) * record_per_page)

        search_result = search_query.all()

        return search_result

    @staticmethod
    def get_total(org_id):
        total_count = None

        if total_count is None:
            total_count = session.query(Result).filter_by(org_id = org_id).count()

        return total_count

    @classmethod
    def _get_by_candidate_ids_and_org_id(cls, candidate_ids, org_id):
        search_query = session.query(Result)

        results = search_query.filter(Result.candidate_id.in_(candidate_ids), Result.org_id == org_id).all()
        return results

    @classmethod
    def get_by_org_id(cls, org_id):
        return session.query(Result).filter_by(org_id = org_id).all()

    @classmethod
    def delete_by_org_id(cls, org_id):
        session.query(Result).filter_by(org_id=org_id).delete()
