import datetime
from numbers import Number

from sqlalchemy import Column, Integer, String, DateTime, Boolean, desc, text
from sqlalchemy.orm import validates

from apps.main.models.base import session, BaseModel


class Organization(BaseModel):
    __tablename__ = 'organization'
    id = Column(Integer, primary_key=True, autoincrement=True)
    legal_name = Column(String(75))
    display_name = Column(String(50))
    slug = Column(String(75), unique=True)
    contact_email = Column(String(50))
    contact_phone = Column(String(10))
    contact_address = Column(String(150))
    city = Column(String(20))
    is_active = Column(Boolean, nullable=False, default=True)
    created_at = Column(DateTime, default=datetime.datetime.now())
    created_by = Column(Integer)  # UserID of the user who created org
    updated_at = Column(DateTime, onupdate=datetime.datetime.now())
    updated_by = Column(Integer)

    @validates('contact_email')
    def validate_contact_email(self, key, value):
        assert value != '', "Contact email cannot be blank"
        return value.lower()

    @validates('legal_name')
    def validate_legal_name(self, key, value):
        assert value != '', "Legal name cannot be blank"
        return value.title()

    @validates('display_name')
    def validate_display_name(self, key, value):
        assert value != '', "Display name cannot be blank"
        return value.title()

    @validates('city')
    def validate_city(self, key, value):
        assert value != '', "City cannot be blank"
        return value.upper()

    @staticmethod
    def get_by_id(org_id):
        return session.query(Organization).filter_by(id=org_id).first()

    @staticmethod
    def get_by_mail(org_mail):
        return session.query(Organization).filter_by(contact_email=org_mail).first()

    @classmethod
    def add(cls, data):
        organization = Organization()
        organization.fill(**data)
        organization.save()
        return organization

    def update_legal_name(self, legal_name):
        self.update(legal_name=legal_name)

    def update_display_name(self, display_name):
        self.update(display_name=display_name)

    def update_contact_phone(self, contact_phone):
        self.update(contact_phone=contact_phone)

    def update_city(self, city):
        self.update(city=city)

    def update_contact_address(self, contact_address):
        self.update(contact_address=contact_address)

    def update_contact_email(self, contact_email):
        self.update(contact_email=contact_email)

    def search_all(self):
        return session.query(Organization).all()

    @classmethod
    def deactivate(cls, organization):
        organization.is_active = False
        organization.save()
        return organization

    @classmethod
    def activate(cls, organization):
        organization.is_active = True
        organization.save()
        return organization

    @classmethod
    def get_total(cls):
        total_count = None

        if total_count is None:
            total_count = session.query(Organization).count()

        return total_count

    @classmethod
    def search(cls, page, record_per_page):
        search_query = session.query(Organization)

        search_query = search_query.order_by(desc(Organization.id))

        if isinstance(page, Number):
            search_query = search_query.limit(record_per_page).offset(
                int(page) * record_per_page)

        search_result = search_query.all()

        return search_result

    @classmethod
    def _get_search_results(cls, query):
        # select * from organisation where organisation.display_name || ' ' || organisation.city || ' ' || organisation.legal_name || ' ' || organisation.email @@ to_tsquery('image');
        organisations = None
        try:
            organisations = session.query(Organization).order_by(desc(Organization.id)).filter(text("organization.display_name || ' ' || organization.city || ' ' || organization.legal_name || ' ' || organization.contact_email || ' ' || organization.slug @@ to_tsquery(:search_string)")).params(search_string=query).all()

        except Exception as e:
            print("__ERROR__ ", e)
        return organisations