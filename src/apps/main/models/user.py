import datetime
import enum
import random
import string

from passlib.hash import pbkdf2_sha256
from sqlalchemy import Column, Integer, String, DateTime, Boolean, text
from sqlalchemy.orm import validates
from sqlalchemy_utils import ChoiceType

from apps.main.models.base import session, BaseModel


class Gender(enum.Enum):
    MALE = 1
    FEMALE = 2
    OTHER = 3


CONST_NO_ID = -1


class User(BaseModel):
    # 'user' is a reserved word in postgres
    __tablename__ = 'users'
    id = Column(Integer, primary_key = True, autoincrement = True)
    first_name = Column(String(40))
    last_name = Column(String(40))
    email = Column(String(50), unique = True)
    mobile_number = Column(String(10))
    gender = Column(ChoiceType(Gender, impl = Integer()), nullable = True)
    password = Column(String(100))
    created_at = Column(DateTime, default = datetime.datetime.now())
    created_by = Column(Integer)
    updated_at = Column(DateTime, onupdate = datetime.datetime.now())
    updated_by = Column(Integer)
    is_active = Column(Boolean, nullable = False, default = True)

    def __repr__(self):
        return u"User(%s, %s)" % (self.first_name, self.email)

    @validates('password')
    def validate_password(self, key, value):
        assert value != '', "Password cannot be blank"
        return value

    @validates('email')
    def validate_email(self, key, value):
        assert value != '', "Email cannot be blank"
        return value

    @classmethod
    def add(cls, data):
        user = User()
        if data.get('password') is None:
            password = User._generate_random_password(6)
            data['password'] = User._hash_password(password)
        user.fill(**data)
        user.save()
        return user, password

    @classmethod
    def authenticate(cls, email, password):
        user = cls.get_by_email(email)
        if user:
            # print(password, user.password)
            if cls._match_password(password, user.password):
                return user
        return None

    @staticmethod
    def _generate_random_password(string_length = 12):
        """Generate a random string of letters, digits and special characters """
        password_characters = string.ascii_letters
        return ''.join(random.choice(password_characters) for i in range(string_length))

    @classmethod
    def _hash_password(cls, password_plain):
        hashed_password = pbkdf2_sha256.encrypt(password_plain, rounds = 200000, salt_size = 16)
        return hashed_password

    @classmethod
    def _match_password(cls, password_plain, hashed_password):
        return pbkdf2_sha256.verify(password_plain, hashed_password)

    @staticmethod
    def search():
        return session.query(User).all()

    @classmethod
    def get_by_id(cls, user_id):
        return session.query(cls).filter(cls.id == user_id).first()

    @staticmethod
    def get_by_email(email):
        return session.query(User).filter_by(email = email).first()

    def update_first_name(self, first_name):
        self.update(first_name = first_name)

    def update_last_name(self, last_name):
        self.update(last_name = last_name)

    def update_email(self, email):
        self.update(email = email)

    def update_mobile_number(self, mobile_number):
        self.update(mobile_number = mobile_number)

    def update_gender(self, gender):
        self.update(gender = gender)

    @classmethod
    def deactivate(cls, user):
        user.is_active = False
        user.save()
        return user

    @classmethod
    def activate(cls, user):
        user.is_active = True
        user.save()
        return user

    @classmethod
    def update_password(cls, user, password):
        user.password = User._hash_password(password)
        user.save()

    @classmethod
    def _full_name(cls, user):
        return str(user.first_name) + ' ' + str(user.last_name)

    @classmethod
    def _get_search_results(cls, query, candidate_ids = None):
        # select * from users where users.first_name || ' ' || users.last_name || ' ' || users.email @@ to_tsquery(query);
        results_users = []
        try:
            results_users = session.query(User).filter(text(
                "users.first_name || ' ' || users.last_name || ' ' || users.email @@ to_tsquery(:search_string)")).params(
                search_string = query).all()

        except Exception as e:
            print("__ERROR__ ", e)

        test_users = []
        if candidate_ids != None:
            for user in results_users:
                if user.id in candidate_ids:
                    test_users.append(user)
        else:
            test_users = results_users

        return test_users

