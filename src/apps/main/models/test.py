import datetime
import enum
from numbers import Number

from sqlalchemy import Column, Integer, JSON, DateTime, String, desc, text
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy_utils import ChoiceType

from apps.main.models.base import BaseModel, session


class Status(enum.Enum):
    NEW = 1
    IN_PROGRESS = 2
    CLOSED = 3
    EXPIRED = 4


class Test(BaseModel):
    __tablename__ = 'test'
    id = Column(Integer, primary_key = True, autoincrement = True)
    candidate_id = Column(Integer)
    total_questions = Column(Integer)
    org_id = Column(Integer)
    question_ids = Column(JSON)

    # meta will have additional details about the created test
    meta = Column(JSON)
    '''
        skill = [{  'skill_id': 2 (Python),
                    'experience_level': 2+ (years)
                },
                {  'skill_id': 4 (Django),
                    'experience_level': 1+ (years)
                },
        ]
    '''
    skill = Column(JSON)
    # In seconds
    total_time = Column(Integer)
    time_left = Column(Integer)
    # Token length = 157 characters
    token = Column(String(200))
    designation = Column(String(200))
    status = Column(ChoiceType(Status, impl = Integer()))
    candidate_profile_picture = Column(String(255))
    ip_address_list = Column(JSON)
    expire_at = Column(DateTime)
    candidate_feedback = Column(String(500))
    created_at = Column(DateTime, default = datetime.datetime.now())
    created_by = Column(Integer)
    updated_at = Column(DateTime, onupdate = datetime.datetime.now())
    updated_by = Column(Integer)

    @classmethod
    def add(cls, data):
        test = Test()
        test.fill(**data)
        test.save()
        return test

    def add_token(self, token):
        self.update(token = token)

    @staticmethod
    def get_by_org(org_id):
        return session.query(Test).filter_by(org_id = org_id).order_by(Test.id.desc()).all()

    @staticmethod
    def get_by_candidate(candidate_id):
        return session.query(Test).filter_by(candidate_id = candidate_id).order_by(Test.id.desc()).all()

    @staticmethod
    def get_by_id(test_id):
        return session.query(Test).filter_by(id = test_id).first()

    def get_by_token(token):
        return session.query(Test).filter_by(token = token).first()

    def update_left_time(self, time_left):
        self.update(time_left = time_left)

    def update_status(self, status):
        self.update(status = status)

    def update_skill(self, skill):
        self.update(skill = skill)

    def update_total_time(self, total_time):
        self.update(total_time = total_time)

    def update_time_left(self, time_left):
        self.update(time_left = time_left)

    def update_total_questions(self, total_questions):
        self.update(total_questions = total_questions)

    def update_expire_date(self, expire_at):
        self.update(expire_at = expire_at)

    def update_designation(self, designation):
        self.update(designation = designation)

    def update_candidate_feedback(self, candidate_feedback):
        self.update(candidate_feedback = candidate_feedback)

    def save_ip_address(self, ip_address):

        # NOTE: Use flag_modified when updating JSON data
        flag_modified(self, 'ip_address_list')

        if self.ip_address_list is None:
            new_ip_address = [ip_address]
            self.ip_address_list = new_ip_address
            self.update(ip_address_list = new_ip_address)
        else:
            all_ips = self.ip_address_list
            new_ips = all_ips + [ip_address]
            self.update(ip_address_list = new_ips)

    def save_candidate_image(self, candidate_profile_picture):
        self.candidate_profile_picture = candidate_profile_picture

    @staticmethod
    def get_total(org_id):
        total_count = None

        if total_count is None:
            total_count = session.query(Test).filter_by(org_id = org_id).order_by(Test.id.desc()).count()

        return total_count

    @staticmethod
    def search(page, record_per_page, org_id):
        search_query = session.query(Test)

        search_query = search_query.filter_by(org_id = org_id).order_by(desc(Test.id))

        if isinstance(page, Number):
            search_query = search_query.limit(record_per_page).offset(
                int(page) * record_per_page)

        search_result = search_query.all()

        return search_result

    @classmethod
    def _get_search_results(cls, query, org_id):
        # select * from test where test.designation || ' ' || test.status @@ to_tsquery('image');
        results_tests = None
        try:
            results_tests = session.query(Test).filter_by(org_id = org_id).order_by(desc(Test.id)).filter(
                text("Test.designation || ' ' || Test.status @@ to_tsquery(:search_string)")).params(
                search_string = query).all()

        except Exception as e:
            print("__ERROR__ ", e)
        return results_tests

    @classmethod
    def delete_by_org_id(cls, org_id):
        session.query(Test).filter_by(org_id = org_id).delete()

    @classmethod
    def get_by_candidate_id_org_id(cls, candidate_id, org_id):
        return session.query(Test).filter_by(candidate_id = candidate_id, org_id = org_id).order_by(
            Test.id.desc()).all()

    @classmethod
    def _get_new_tests(cls):
        return session.query(Test).filter_by(status = Status.NEW).all()
