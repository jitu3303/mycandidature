from sqlalchemy import Column, Integer, String, BIGINT, text
from datetime import datetime
# from .base import Base
from sqlalchemy.orm import validates

from apps.main.models.base import session, BaseModel


class Skill(BaseModel):
    __tablename__ = 'skill'
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(30))
    slug = Column(String(30), unique=True)
    deactivated_at = Column(BIGINT, nullable = True, default = datetime.now().timestamp())

    @validates('title')
    def validate_title(self, key, value):
        assert value != '', "Skill cannot be blank"
        return value

    @classmethod
    def add(cls, data):
        skill = Skill()
        skill.fill(**data)
        skill.save()

    @staticmethod
    def list_all():
        return session.query(Skill).all()

    @classmethod
    def get_by_slug(cls, slug):
        return session.query(Skill).filter(cls.slug == slug).first()

    @classmethod
    def get_by_id(cls, skill_id):
        return session.query(Skill).filter(cls.id == skill_id).first()

    @staticmethod
    def get_by_ids(skill_ids):
        query = session.query(Skill.id, Skill.title).filter(Skill.id.in_(skill_ids))
        return query.all()

    def update_title(self, title):
        self.update(title=title)

    def update_slug(self, slug):
        self.update(slug=slug)

    def update_deactivated_at(self, deactivated_at):
        self.update(deactivated_at=deactivated_at)

    @classmethod
    def _get_search_results(cls, query):
        # select * from skill where to_tsvector(skill.title) @@ to_tsquery('image');
        results_skills = None
        try:
            results_skills = session.query(Skill).filter(text("Skill.title @@ to_tsquery(:search_string)")).params(search_string=query).all()

        except Exception as e:
            print("__ERROR__ ", e)

        return results_skills
