
// Capture Candidate Photo's global vars:
var width = 320;    // We will scale the photo width to this
var height = 0;     // This will be computed based on the input stream
var streaming = false;
var video = null;
var canvas = null;
var photo = null;
var capturePhotoBtn = null;

APP.prototype.captureCandidatePhoto = function() {
    video = document.getElementById('video');
    canvas = document.getElementById('canvas');
    photo = document.getElementById('photo');
    capturePhotoBtn = document.getElementById('capturePhotoBtn');
    streaming = false;

    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
    .then(function(stream) {
        video.srcObject = stream;
        video.play();
    })
    .catch(function(err) {
        console.log("An error occurred: " + err);
    });

    video.addEventListener('canplay', function(ev){
        if (!streaming) {
            height = video.videoHeight / (video.videoWidth/width);
            video.setAttribute('width', width);
            video.setAttribute('height', height);
            // Initially canvas' height width = 0
            canvas.setAttribute('width', 0);
            canvas.setAttribute('height', 0);
            streaming = true;
        }
    }, false);

    capturePhotoBtn.addEventListener('click', function(ev){
        ev.preventDefault();
        app.takePicture();
    }, false);

}

APP.prototype.takePicture = function() {
    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
    .catch(function(err) {
        if(err=="NotFoundError: Requested device not found") {
            alert("Please attach webcam");
        }
    });
    var context = canvas.getContext('2d');
    if (width && height) {
      canvas.width = width;
      canvas.height = height;
      context.drawImage(video, 0, 0, width, height);
      var data = canvas.toDataURL('image/png');
      $('#candidate_photo').val(data);
      $('#submit_test').removeAttr("disabled");
    }
};

APP.prototype.initPage = function () {
    app.captureCandidatePhoto();
};

$(document).ready(function () {
    app.initPage();
});
