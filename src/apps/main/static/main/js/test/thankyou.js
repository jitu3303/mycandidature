APP.prototype.disableBackButton = function () {
    window.history.pushState(null, "", window.location.href);
    window.onpopstate = function () {
        window.history.pushState(null, "", window.location.href);
    };
};

APP.prototype.initPage = function () {
    app.disableBackButton();
};

$(document).ready(function () {
    app.initPage();
});
