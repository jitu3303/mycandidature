APP.prototype.__handleTimer = function () {
    var timerElement = document.getElementById("timer");
    var timerInput = document.getElementById("timeLeft");

    app.timer = setInterval(function () {
        var a = new Date();
        if (app.seconds <= 0) {
            clearInterval(app.timer);
            window.location = '/test/thankyou'
            // alert("Time's Up");
            // window.location.href = "/test/thankyou";
            //
        }
        timerElement.innerText = app.seconds.toString();
        timerInput.value = app.seconds;
        app.seconds--;
        app.__updateTimer();
    }, 1000);
};

APP.prototype.__updateTimer = function () {
    $.ajax({
        type: "POST",
        url: "/test/update_timer",
        data: {
            'csrfmiddlewaretoken': $('meta[name="csrf-token"]').attr("content"),
            'time_left': $('#timeLeft').val(), // from form,
            'test_id': $('#testID').val()      // test ID
        },
        success: function () {
            $('#message').html("<h2>Contact Form Submitted!</h2>")
        }
    });
    return false; //<---- move it here
};

APP.prototype.validateForm = function () {
    let $testForm = $("#testForm");
    let defaultOptions = app.defaultValidationOptions();
    $testForm.parsley(defaultOptions);
};

APP.prototype.disableControls = function () {
    document.oncontextmenu = new Function("return false;");
    document.oncopy = new Function("return false;");
    document.oncut = new Function("return false;");
    document.onpaste = new Function("return false;");
    document.onselectstart = new Function("return false;");
    app.disableBackButton();
    app.disableCtrlKeys();
};

APP.prototype.disableCtrlKeys = function () {
    window.addEventListener("keydown", function (e) {
        // Ctrl + f
        // 123 for developer tools
        if (e.keyCode === 114 || (e.ctrlKey && e.keyCode === 70) ||
            e.keyCode === 112 || e.keyCode === 117 || e.keyCode === 121 ||
            e.keyCode === 27 || e.keyCode === 122 || (e.ctrlKey && e.keyCode === 83) ||
            (e.ctrlKey && e.keyCode === 67) || (e.ctrlKey && e.keyCode === 86) ||
            (e.ctrlKey && e.keyCode === 74) || e.keyCode === 123 ||
            (e.metaKey && e.keyCode === 83) || (e.metaKey && e.keyCode === 67) ||
            (e.metaKey && e.keyCode === 86) || (e.metaKey && e.keyCode === 74) ||
            (e.metaKey && e.keyCode === 70)) {
            e.preventDefault();
        }
    });
};

APP.prototype.disableBackButton = function () {
    window.history.pushState(null, "", window.location.href);
    window.onpopstate = function () {
        window.history.pushState(null, "", window.location.href);
    };
};

/* APP.prototype.notifyOnWindowClose = function () {
    // window.onbeforeunload = function (e) {
    //     e.preventDefault();
    //     e.returnValue = "Are you sure you want to end the test?";
    // }
    // app.nextPressed = false;
    // nextBtn = document.querySelector("#next");
    // nextBtn.addEventListener("mousedown", function (e) {
    //     app.nextPressed = true;
    //     console.log("App press " + app.nextPressed)
    // });


    window.addEventListener("beforeunload", function (e) {
        var confirmationMessage = 'Are you sure you want to end the test?'
            + 'Your answer will be lost if you leave before submitting.';

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE

        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    });

}; */


APP.prototype.initPage = function () {
    var timerElement = document.getElementById("timer");

    app.seconds = Number(timerElement.innerText);
    app.__handleTimer();
    app.validateForm();
    app.disableControls();
    // Shows the notification even when pressing next
    // app.notifyOnWindowClose();
};

$(document).ready(function () {
    app.initPage();
});
