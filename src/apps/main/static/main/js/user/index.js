APP.prototype.validateForm = function () {
    var $addForm = $("#passwordchangeform");
    var defaultOptions = app.defaultValidationOptions();
    $addForm.parsley(defaultOptions);
};

APP.prototype.initPage = function () {
    app.validateForm();
};


$(document).ready(function () {
    app.initPage();
});