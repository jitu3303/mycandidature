 APP.prototype.__validateForm = function () {
    var $addForm = $("#addForm");
    var defaultOptions = app.defaultValidationOptions();
    window.Parsley.addValidator('min6', {
        validateString: function (value, requirement, instance) {
            	var notice =$('#notice').html(' ');
                var group = $(requirement);
                var fieldsEmpty = 0;
                var fieldsNotEmpty = 0;
                var count = 0
                group.each(function () {
                    var _val = $(this).val()
                        console.log(_val);
                    var length = _val.length
                    if (length > 0) {
                        fieldsNotEmpty++;
                    }
                    else {
                        fieldsEmpty++;
                    }
                    count++;
                })
                var isValid = (fieldsNotEmpty >=1)
                return isValid;
        }
    });
	$(function () {
		var ok=false;
		var notice =$('#notice');
		$('#addForm').parsley().on('form:validated', function(formInstance) {
		    ok = formInstance.isValid({force: true});
		})
		.on('form:submit', function() {
    		if(!ok){
    			notice.html('Please select at least 1 field');
    			return false;
    		}
    		else{
    			notice.html(' ');
    			return true;
    		}
  		});
	});
    $addForm.parsley(defaultOptions);
};

APP.prototype.__handleQuestionTypeChange = function () {
    $("#questionType").on('change', function () {
        let selectedOption = document.getElementById("questionType").value;
        console.log("selectedOption", selectedOption);
        if (selectedOption === 'descriptive') {
            let descriptionContent = '<div class="form-group">\n' +
                '                    <label for="answer_description">Answer Description</label>\n' +
                '                    <textarea id="answer_description" type="text" class="form-control"' +
                ' name="answer_description"\n' +
                '                          required="required" placeholder=""\n' +
                '                          rows="3"></textarea>\n' +
                '                </div>';
            generatedDiv.innerHTML = descriptionContent;
            plusButton.innerHTML = '';
        } else if (selectedOption === 'single_correct' || selectedOption === 'multiple_correct') {
            index = 0;
            generatedDiv.innerHTML = '';
            multiple_correct = selectedOption === 'multiple_correct';
            generateDynamicAnswers();
        }
    });

}

APP.prototype.initPage = function () {
    app.__validateForm();
    app.__handleQuestionTypeChange();
};

$(document).ready(function () {
    app.initPage();
});


let index = 0;
let multiple_correct;
let plusButton = document.getElementById("plusBtn");
let generatedDiv = document.getElementById("ansCnt");

plusButton.onclick = generateDynamicAnswers;

function generateDynamicAnswers() {
    if(index<4) {
    multiple_correct ? addCheckboxClone() : addRadioClone();
    // generatedDiv.innerHTML = optionContent;
    console.log("Index is --> ", index);
    plusButton.innerHTML = '<a class="btn btn-outline-secondary btn-sm mt-3"> + </a>';
    index = index + 1;
    } else {
        alert("Cannot add more than 4 options")
    }
}

function addRadioClone() {
    let generatedDiv = document.getElementById("ansCnt");
    let optionContent = createRadioElement(index);
    // generatedDiv.innerHTML += optionContent;
    let newDiv = document.createElement('div');
    newDiv.innerHTML = optionContent;
    generatedDiv.appendChild(newDiv);
}

function addCheckboxClone() {
    let generatedDiv = document.getElementById("ansCnt");
    let optionContent = createCheckboxElement(index);
    // generatedDiv.innerHTML += optionContent;
    let newDiv = document.createElement('div');
    newDiv.innerHTML = optionContent;
    generatedDiv.appendChild(newDiv);
}

function createRadioElement() {
    let radioElem = '<div class="form-check mt-2">\n' +
        '                    <input type="radio" class="form-check-input" name="checks" value=' + index + '>\n' +
        '                    <input type="text" class="form-control"  name="answer_fields" required/>\n' +
        '                </div>'
    return radioElem;
}

function createCheckboxElement() {
    let checkboxElem = '<div class="form-check mt-2">\n' +
        '                    <input type="checkbox" class="form-check-input" name="checks" value=' + index + '>\n' +
        '                    <input type="text" class="form-control" name="answer_fields" required/>\n' +
        '                </div>'
    return checkboxElem;
}