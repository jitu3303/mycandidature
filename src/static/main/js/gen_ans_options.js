let index = 0;
let multiple_correct;
let plusButton = document.getElementById("plusBtn");
let generatedDiv = document.getElementById("ansCnt");

plusButton.onclick = generateDynamicAnswers;

function onQsTypeSelect() {
    let selectedOption = document.getElementById("question_type").value;

    if (selectedOption === 'descriptive') {
        let descriptionContent = '<div class="form-group">\n' +
            '                    <label for="answer_description">Answer Description</label>\n' +
            '                    <textarea id="answer_description" type="text" class="form-control"' +
            ' name="answer_description"\n' +
            '                          required="required" placeholder="Enter Description of the Answer here"\n' +
            '                          rows="3"></textarea>\n' +
            '                </div>';
        generatedDiv.innerHTML = descriptionContent;
        plusButton.innerHTML = '';
    } else if (selectedOption === 'single_correct' || selectedOption === 'multiple_correct') {
        index = 0;
        generatedDiv.innerHTML = '';
        multiple_correct = selectedOption === 'multiple_correct';
        generateDynamicAnswers();
    }
}

function generateDynamicAnswers() {
    multiple_correct ? addCheckboxClone() : addRadioClone();
    // generatedDiv.innerHTML = optionContent;
    console.log("Index is ", index);
    plusButton.innerHTML = '<a class="btn btn-outline-primary mt-3"> + </a>';
    index = index + 1;
}

function addRadioClone() {
    let generatedDiv = document.getElementById("ansCnt");
    let optionContent = createRadioElement(index);
    // generatedDiv.innerHTML += optionContent;
    let newDiv = document.createElement('div');
    newDiv.innerHTML = optionContent;
    generatedDiv.appendChild(newDiv);
}

function addCheckboxClone() {
    let generatedDiv = document.getElementById("ansCnt");
    let optionContent = createCheckboxElement(index);
    // generatedDiv.innerHTML += optionContent;
    let newDiv = document.createElement('div');
    newDiv.innerHTML = optionContent;
    generatedDiv.appendChild(newDiv);
}

function createRadioElement() {
    let radioElem = '<div class="form-check mt-2">\n' +
        '                    <input type="radio" class="form-check-input" name="checks" value=' + index + '>\n' +
        '                    <input type="text" class="form-control"  name="answer_fields" />\n' +
        '                </div>'
    return radioElem;
}

function createCheckboxElement() {
    let checkboxElem = '<div class="form-check mt-2">\n' +
        '                    <input type="checkbox" class="form-check-input" name="checks" value=' + index + '>\n' +
        '                    <input type="text" class="form-control" name="answer_fields"/>\n' +
        '                </div>'
    return checkboxElem;
}